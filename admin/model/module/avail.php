<?php
class ModelModuleAvail extends Model {	
	
	public function getAvailabilities($data = array()) {	
			
		//	$decrypted = $cypher->decrypt(pack("H*",$license['license']));
			
			
			
				  
				if ($data) {
				
					$sql = "SELECT a.*,p.quantity FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '0' ";


					$sort_data = array(
						'id',
						'status'
					);

					if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
						$sql .= " ORDER BY " . $data['sort'];
					} else {
						$sql .= " ORDER BY id";
					}

					if (isset($data['order']) && ($data['order'] == 'DESC')) {
						$sql .= " DESC";
					} else {
						$sql .= " ASC";
					}

					if (isset($data['start']) || isset($data['limit'])) {
						if ($data['start'] < 0) {
							$data['start'] = 0;
						}

						if ($data['limit'] < 1) {
							$data['limit'] = 20;
						}

						$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
					}

					$query = $this->db->query($sql);

					return $query->rows;
				} else {

					$query = $this->db->query("SELECT a.*,p.quantity FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '0' ");
					$option_val = array();
					$option_latest = array();
					
					foreach ($query->rows as $option) {
										
					$option_latest[] = array (
					       'id' => $option['id'],
						   'time' => $option['time'],
						   'email' => $option['email'],
						   'admin_email' => $option['admin_email'],
						   'product_id' => $option['product_id'],
						   'price' => $option['price'],
						   'link_page' => $option['link_page'],
						   'name' => $option['name'],
						   'comment' => $option['comment'],
						   'status' => $option['status'],
						   'quantity' => $option['quantity'],
						   'product' => $option['product'],
						   'language_id' => $option['language_id'],
						   'option' => $this->OptionBuyProduct($option['product_id'],$option['id'])
						);					
					}	                 
					return $option_latest;					 
				}				
			
		
        
    }	
	
    public function getProcessed($data = array()) {
        if ($data) {
            $sql = "SELECT a.*,p.quantity  FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '1' ";


            $sort_data = array(
                'id',
                'status'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
			$query = $this->db->query("SELECT a.*,p.quantity FROM " . DB_PREFIX . "avail a, " . DB_PREFIX . "product p WHERE a.product_id = p.product_id AND a.status = '1' ");
					$option_val = array();
					$option_latest = array();
					
					foreach ($query->rows as $option) {
										
					$option_latest[] = array (
					       'id' => $option['id'],
						   'time' => $option['time'],
						   'email' => $option['email'],
						   'admin_email' => $option['admin_email'],
						   'product_id' => $option['product_id'],
						   'price' => $option['price'],
						   'link_page' => $option['link_page'],
						   'name' => $option['name'],
						   'comment' => $option['comment'],
						   'status' => $option['status'],
						   'quantity' => $option['quantity'],
						   'product' => $option['product'],
						   'option' => $this->OptionBuyProduct($option['product_id'],$option['id'])
						);					
					}	
                   
					return $option_latest;
        }
    }
	public function getProductQuantity($product_id) {
        $query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product where product_id = " . $product_id);
		
		foreach ( $query->row as $key => $value ) {
			return $value;
		}        
    }
	public function notify() {

        $query = $this->db->query("select * from " . DB_PREFIX . "product p, " . DB_PREFIX. "avail a where p.quantity > 0 AND p.product_id = a.product_id AND a.status = 0");
		 
        return $query->rows;
    }	
	/*товары без опций*/
	public function notifyOption($product_id = null) {

				$sql = "select distinct(a.id), a.name, a.language_id, a.product, p.quantity, p.price, a.product_id, 
				a.email
					from " . DB_PREFIX . "product p, " . DB_PREFIX . "avail a		
					where a.id not in (SELECT  distinct(main_id) FROM " . DB_PREFIX . "avail_options ao)
					AND p.quantity > 0 
					AND p.product_id = a.product_id ";
				if (!is_null($product_id)){	
					$sql .= "AND a.product_id = " . $product_id;
				}
				$sql .=" AND a.status = 0 group by a.id";
					
		        $query = $this->db->query($sql);

        return $query->rows;
    }	
	public function ProductWithOption($product_id = null) {

				$sql = "SELECT DISTINCT (
						a.id
						), a.product, a.language_id, a.name, a.email, p.quantity, p.price,a.product_id 
						FROM " . DB_PREFIX . "product p, " . DB_PREFIX . "avail a, " . DB_PREFIX . "avail_options ao
						WHERE a.id = ao.main_id
						AND p.product_id = a.product_id";
				if (!is_null($product_id)){	
				$sql .= " AND a.product_id = ".$product_id;
				}
				$sql .=	" AND a.status = 0";

        $query = $this->db->query($sql);
		
        return $query->rows;
    }	
	public function OptionBuyProduct($product_id, $notify_id = "") {
                 
				$sql = "SELECT pov.quantity as option_quantity, ao.option_type, ao.option_name  
					FROM " . DB_PREFIX . "avail_options ao, " . DB_PREFIX . "product_option_value pov ";				
				$sql .= "WHERE ao.product_id = " . $product_id . "
					AND ao.option_value_id = pov.option_value_id
					AND pov.product_id = " .$product_id . "";
				if(!empty($notify_id)) {
				$sql .= " AND ao.main_id = " . $notify_id . " AND pov.product_option_value_id = ao.product_option_value_id";
				
				}	
        $query = $this->db->query($sql);
        return $query->rows;
    }	
	public function notifyByProductId($product_id) {
		$result = $this->model_module_avail->notifyOption($product_id);
		$result_options = $this->model_module_avail->ProductWithOption($product_id);
		$messages = $this->config->get('avail');		
		$this->language->load('module/avail');
		$link = HTTP_CATALOG . 'index.php?route=product/product&product_id='. $product_id;		
		
		if(!empty($result)){			
				foreach ($result as $info ) {
					$link = HTTP_CATALOG . 'index.php?route=product/product&product_id='. $info['product_id'];
					
					$search_data = array('%name%', '%product_name%', '%price%', '%link%','%option_type%','%option_name%');
					$replace_data = array($info['name'], $info['product'], $info['price'], "<a href=" . $link . ">" . $info['product'] . "</a>",'','');
					
					if ($messages) {						
						if(!empty($messages['notification_message'][$info['language_id']]))
						$mail_text = htmlspecialchars_decode($messages['notification_message'][$info['language_id']]);
						$mail_text = str_replace($search_data, $replace_data, $mail_text);
					} else {
						$mail_text = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Document</title></head><body>";
						$mail_text .= "<p>" . html_entity_decode($info['name'].', '.$this->language->get('text_mail_send'). "</p>");
						$mail_text .= "<p>" . $this->language->get('text_product') .': ' . $info['product'] . "</p>";
						$mail_text .= "<p>" . $this->language->get('text_link_page') . ": " . " <a href=" . $link . ">" . $info['product'] . "</a></p>";
						$mail_text .= "<p>" . $this->language->get('text_price') . ': ' . $info['price'] . "</p></body></html>";
					}					
					
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->hostname = $this->config->get('config_smtp_host');
					$mail->username = $this->config->get('config_smtp_username');
					$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->port = $this->config->get('config_smtp_port');
					$mail->timeout = $this->config->get('config_smtp_timeout');
					
					$mail->setTo($info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender('\''.$this->language->get('email_subject').'\'');
					$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $info['name'], ENT_QUOTES, 'UTF-8')));				

					$mail->setHtml($mail_text);
					$mail->send();
					$this->model_module_avail->changeMailStatus($info['id'], 1);
				}
			}
		if (!empty($result_options)){
				$option_types = '';
				$option_names = '';				
			    foreach ($result_options as $info ) {					
					$flag = 0;	
					$link = HTTP_CATALOG . 'index.php?route=product/product&product_id='. $info['product_id'];
			        /*масив данными по товаре и его опциях по заявке*/
					$options_notify = $this->model_module_avail->OptionBuyProduct($info['product_id'], $info['id']);
					
					$info['options'] = array();
					array_push($info['options'], $options_notify);
					
				   if ($this->config->get('avail_options_status') == 1){
						foreach ($info['options'] as $index => $arr ) {				
							foreach($arr as $option) {
								if($option['option_quantity'] <= 0) {
									$flag = 1;									
								}						
							}						
						}
					} else { 
						if ($info['quantity'] <= 0){
							$flag = 1;
					    }
					
					}
					
					if($flag == 0) {						
						foreach($info['options'] as $index => $arr ) {
							$i =0; 
							
							foreach($arr as $option) {
								if($i == 0){ $mark = " ";}else{ $mark = " , ";}
								$option_names .=  $mark . "" . $option['option_type'] . " - ". $option['option_name'] ;
							$i++;}
						}
					$search_data = array('%name%', '%product_name%', '%price%', '%link%','%option_type%','%option_name%');
					$replace_data = array($info['name'], $info['product'], $info['price'], "<a href=" . $link . ">" . $info['product'] . "</a>", '' , $option_names);						
						if ($messages) {
							if(!empty($messages['notification_message'][$info['language_id']])) {
								$mail_text = htmlspecialchars_decode($messages['notification_message'][$info['language_id']]);
								$mail_text = str_replace($search_data, $replace_data, $mail_text);						
							}							
						} else {
							$mail_text = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Document</title></head><body>";
							$mail_text .="<p>" . html_entity_decode($info['name'].', '.$this->language->get('text_mail_send'). "</p>");
							$mail_text .= "<p>" . $this->language->get('text_product') .': ' . $info['product'] . "</p>";
							$mail_text .= "<p>" . $this->language->get('text_link_page') . ": " . " <a href=" . $link . ">" . $info['product'] . "</a></p>";
							$mail_text .= "<p>" . $this->language->get('text_price') . ': ' . $info['price'] . "</p>";
							foreach($info['options'] as $index => $arr ) {
								foreach($arr as $option) {
									$mail_text .= "<p>" . $option['option_type'] . ' - ' . $option['option_name'] . "</p>";
								}								
							}	
							$mail_text .= "</body></html>";
						}
							$mail = new Mail();
							$mail->protocol = $this->config->get('config_mail_protocol');
							$mail->parameter = $this->config->get('config_mail_parameter');
							$mail->hostname = $this->config->get('config_smtp_host');
							$mail->username = $this->config->get('config_smtp_username');
							$mail->password = html_entity_decode($this->config->get('config_smtp_password'), ENT_QUOTES, 'UTF-8');
							$mail->port = $this->config->get('config_smtp_port');
							$mail->timeout = $this->config->get('config_smtp_timeout');
										
							$mail->setTo($info['email']);
							$mail->setFrom($this->config->get('config_email'));
							$mail->setSender('\''.$this->language->get('email_subject').'\'');
							$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $info['name'], ENT_QUOTES, 'UTF-8')));				
							
							$mail->setHtml($mail_text);
								
							$mail->send();
							$this->model_module_avail->changeMailStatus($info['id'], 1);
						
					}
									
				}
            }				
		
	}
     public function getAvailability($id) {

         if ($id) {
             $sql = "SELECT * FROM " . DB_PREFIX . "avail where id = ". $id ."";


             $query = $this->db->query($sql);

             return $query->row;
         }
     }


    public function getTotalCalls() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "avail");

        return $query->row['total'];
    }
    
    public function getTotalAvail() {
	 $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "avail WHERE status = '0'");

        return $query->row['total'];
	}
    
    public function changeMailStatus($id, $status) {
        if ($this->db->query("UPDATE " . DB_PREFIX . "avail SET status = '" . (int) $status . "' WHERE id = '" . (int) $id . "'")) {
            return true;            
        } else {
            return false;
        }
    }
	public function deleteNotifications($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "avail WHERE id = '" . (int)$id . "'");		
		
	}
	
    public function install() {

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "avail` (
            `id` int(6) NOT NULL AUTO_INCREMENT,
            `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `email` varchar(50) NOT NULL,
			`admin_email` varchar(50) NOT NULL,
			`product` varchar(50) NOT NULL,
			`product_id` int(11) NOT NULL,
			`price` varchar(50) NOT NULL,
            `link_page` varchar(255) NOT NULL,
			`name` varchar(50) NOT NULL,
            `comment` text NOT NULL,
            `status` varchar(50) NOT NULL,
			`language_id` int(3) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		  $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "avail_options` (
			`id` int(6) NOT NULL AUTO_INCREMENT,
			`main_id` int(6) NOT NULL,
			`product_id` int(11) NOT NULL,
			`option_value_id` int(11) NOT NULL,
			`option_quantity` int(11) NOT NULL,
			`option_name` varchar(50) NOT NULL,
			`option_type` varchar(50) NOT NULL,
			`product_option_value_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
	
}

}
?>