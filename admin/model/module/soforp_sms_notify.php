<?php
require_once( DIR_SYSTEM . "/engine/soforp_model.php");

class ModelModuleSoforpSmsNotify extends SoforpModel {

    private $error = array();

    public function __construct($registry) {
        parent::__construct($registry);
        $this->_moduleSysName = "soforp_sms_notify";
        $this->_logFile = $this->_moduleSysName . ".log";
        $this->debug = $this->config->get( $this->_moduleSysName . "_debug");
    }

    protected function install(){

        return TRUE;
    }

    protected function uninstall(){

        return TRUE;
    }
}
