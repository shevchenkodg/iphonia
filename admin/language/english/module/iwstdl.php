<?php

// Heading
    $_['heading_title'] = 'Todo list [<a href="http://ibazh.com" target="_blank">ibazh.com</a>]';

// Text
    $_['text_module'] = 'Modules';
    $_['text_success'] = 'Success: You have modified Todo list!';
    $_['text_todo'] = '
       <img src = "view/image/tdl_0.png"/ style="float:right;">
       
<p>To do list - an organizer for tasks ocStore (Opencart), making work easy and convenient to help you remember 
    important tasks. The simple interface makes managing enjoyable. </ p> 

<p> To do list includes the following features: </ p> 
<ul> 
    <li> Highlight completed assignments </ li> 
    <li> Add deadlines tasks </ li> 
    <li> Delete job </ li> 
    <li> Start button, To do list on the main page of the admin panel (just shows the number of outstanding 
jobs in admin panel). </ li> 
</ ul> 

<p>
Compatibility: <br> 
ocStore v1.5.4, v1.5.5.1.1, v1.5.5.1.2 <br> 
Opencart v1.5.6.x <br> </p>

Tested in browsers: 
<ul> 
    <li> - Chrome Version 36.0.1985.125 m </ li> 
    <li> - FF 31.0 </ li> 
    <li> - Opera 12.14 </ li> 
</ ul>';
    $_['text_taskleft'] = 'Remaining task';
    $_['text_daysleft'] = 'Days left';
    $_['text_taskadd'] = 'Add task';
    $_['text_taskdel'] = 'Delete task';
    $_['text_taskcal'] = 'Press to select the end date of the task';
    $_['button_nazad'] = 'Back';
    $_['button_vyhod'] = 'Cancel';
    $_['button_sohr'] = 'Save';


// Error
    $_['error_permission'] = 'Warning: You do not have permission to modify module latest!';
?>