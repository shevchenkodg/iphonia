<?php
// Heading
$_['heading_title']       = 'Custom Success Page';
$_['heading_normal']      = 'Настройки';
$_['heading_redirect']    = 'Перейти на страницу';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Успех: Вы модифицировали пользовательский модуль успеха Page!';
$_['text_shortcode']      = '{order_id} - order ID<br />{email} - E-mail клиент<br />{date_order} - Дате добавления заказа<br />{shipping_method} - способ доставки<br />{delivery_address} - адрес доставки<br />{facebook} - Like box<br />{is_logged} - сообщение, если клиент регистрируется';

// Entry
$_['entry_layout']           = 'планировка нагрузки:';
$_['entry_facebook']         = 'Facebook:';
$_['entry_dimension']        = 'Размеры коробки:';
$_['entry_facebook_profile'] = 'Profile ID:';
$_['entry_facebook_border']  = 'Border color:<br /><span class="help">Нажмите здесь для <a href="http://www.colorpicker.com/" target="_blank">color picker</a>.<br>e.g. #ffffff</span>';
$_['entry_redirect']         = 'Redirect url:<br /><span class="help">Please include http:// or https:// в начале url.</span>';
$_['entry_status']           = 'Status:';
$_['entry_to_default']       = 'Добавить default tamplate:<br /><span class="help">Добавить содержимое чуть ниже на странице успеха умолчанию.</span>';
$_['entry_body']             = 'Установите шаблон';
$_['entry_logged']           = 'Дополнительный текст для зарегистрированных клиентов (чтобы они были видны в окне выше, вы должны использовать код{is_logged})';

// Error
$_['error_permission']    = 'Внимание: Вы не имеете разрешения на изменение пользовательский модуль успеха Page!';
?>