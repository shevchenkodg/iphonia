<?php

$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo SMS Информер</p>';
$_['heading_title_raw'] = 'NeoSeo SMS Информер';

$_['column_status_name'] = 'Название статуса';
$_['column_template_subject'] = 'Сообщение';
$_['tab_templates_desc'] = 'Укажите сообщения согласно статусам заказов';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_select_template'] = 'Укажите шаблон';
$_["text_new_order"] = 'Получен новый заказ';
$_['text_customer_templates'] = 'Сообщения покупателям';
$_['text_admin_templates'] = 'Сообщения админам';
$_['text_module_version'] = '';

$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_insert'] = 'Создать';
$_['button_delete'] = 'Удалить';
$_['button_clear_log'] = 'Очистить лог';


$_['field_desc_order_status'] = 'Статус заказа';
$_['field_desc_order_date'] = 'Дата заказа';
$_['field_desc_date'] = 'Текущая дата';
$_['field_desc_total'] = 'Итоги заказа (total, currency_code, currency_value)';
$_['field_desc_sub_total'] = 'Промежуточный итоги заказа (currency_code, currency_value)';
$_['field_desc_invoice_number'] = 'Номер счета';
$_['field_desc_comment'] = 'Примечание к заказу';
$_['field_desc_shipping_cost'] = 'Стоимость заказа';
$_['field_desc_tax_amount'] = 'Сумма налога';
$_['field_desc_logo_url'] = 'Ссылка на магазин';


// Entry
$_['entry_status'] = 'Статус:';
$_['entry_status_desc'] = 'Отправлять сообщения при изменении статуса заказа.';
$_['entry_force'] = 'Форсировать:';
$_['entry_force_desc'] = 'Отсылать смс даже если не включена опция "уведомить покупателя":';
$_['entry_recipients'] = 'Получатели админских сообщений:';
$_['entry_recipients_desc'] = 'Укажите номера телефонов в международном формате через запятую.';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = 'В системные логи будет писаться различная информация для разработчика модуля.';
$_['entry_gate'] = 'SMS-шлюз';
$_['entry_gate_login'] = 'Логин для SMS-шлюза';
$_['entry_gate_password'] = 'Пароль для SMS-шлюза';
$_['entry_gate_sender'] = 'Отправитель для SMS-шлюза';
$_['entry_gate_additional'] = 'Дополнительные параметры для SMS-шлюза';
$_['entry_gate_check'] = 'Проверка ';
$_['entry_gate_check_phone'] = "Телефон";
$_['entry_gate_check_message'] = "Сообщение";
$_['entry_field_list_name'] = 'Шаблон';
$_['entry_field_list_desc'] = 'Описание';
$_['entry_align'] = 'Автодополнение:';
$_['entry_align_desc'] = 'Для правильной работы шлюза нужен полный номер телефона, например 38 095 111 11 11, но покупатели часто вводят только часть, например 095 111 11 11. Укажите маску полного значения, например 38 000 000 00 00, и модуль сам дополнит введенный номер недостающими числами';

$_['tab_general'] = 'Общее';
$_['tab_templates'] = 'Шаблоны сообщений';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';


// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_ioncube_missing'] = '<h3 style="color:red">Отсутствует IonCube Loader!</h3><p>Чтобы пользоваться нашим модулем, вам нужно установить IonCube Loader. Ниже приводятся инструкции по установке IonCube Loader для разных случаев:</p><ul><li>Если у вас shared-хостинг - <a href="http://neoseo.com.ua/articles/ioncube-loader-shared">http://neoseo.com.ua/articles/ioncube-loader-shared</a></li><li>Если у вас VPS на ubuntu - <a href="http://neoseo.com.ua/articles/ioncube-loader-ubuntu">http://neoseo.com.ua/articles/ioncube-loader-ubuntu</a></li><li>Если у вас VPS на centos - <a href="http://neoseo.com.ua/articles/ioncube-loader-centos">http://neoseo.com.ua/articles/ioncube-loader-centos</a></li></ul><p>Если у вас не выходит установить IonCube Loader самостоятельно, вы также можете попросить помощи у наших специалистов по адресу <a href="mailto:license@neoseo.com.ua">license@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p>';
$_['error_license_missing'] = '<h3 style="color:red">Отсутствует файл лицензии!</h3><p>Для получения файла лицензии свяжитесь с разработчиком модуля по адресу <a href="mailto:license@neoseo.com.ua">license@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p><p>Полученный файл лицензии положите в корень сайта, т.е. рядом с файлом robots.txt и нажмите кнопку "Проверить еще раз".</p><p>Вы можете не переживать что ваш файл лицензии кто-то украдет! Ваш файл лицензии сделан персонально для вас и не будет работать на другом домене</p>';

$_['mail_support'] = '
                      <p>Если у вас возникли проблемы с установкой или настройкой модуля, вы можете запросить поддержку по адресу <a href="mailto:license@neoseo.com.ua">license@neoseo.com.ua</a></p>
                      <p><a href="http://neoseo.com.ua">Веб-студия NeoSeo</a> создает не только прекрасные модули для opencart, но и онлайн бизнес под ключ.</p>
                      ';
$_['module_licence'] = '
        <p>Покупатель модуля имеет право на использование модуля на одном домене.</p>
        <p>Все права на модуль принадлежат <a href="http://neoseo.com.ua">Веб студии NeoSeo</a>. Запрещена передача данного ПО третьим лицам, распространение от своего имени без получения разрешения автора модуля. Запрещается публикация, распространение модуля без согласия автора в любых целях, будь то ознакомительных или любых других.</p>
        <p><a href="http://neoseo.com.ua">Веб-студия NeoSeo</a> создает не только прекрасные модули для opencart, но и онлайн бизнес под ключ.</p>
        ';
$_['text_module_version']='v.18';
