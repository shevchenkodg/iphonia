<?php

$_['heading_title'] 			= 'Availability';
$_['text_module']	 			= 'Modules';
$_['entry_name']       = 'Module Name';

$_['text_success_avail']				= 'Success! You have modified Avail';
$_['text_view_notices'] = 'View Notices';

$_['text_capcha'] = 'Capcha';


$_['entry_status']     = 'Status:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_sort_order']    = 'Sort Order:';
$_['text_content_top'] = 'Top content';
$_['text_content_bottom'] = 'Bottom content';
$_['text_column_left'] = 'Left column';
$_['text_column_right'] = 'Right column';
$_['text_license'] = 'Insert your license key';
$_['text_options_status'] = 'Consider options on the product';
//Capcha

$_['entry_capcha_status'] = 'Captcha Status';
$_['entry_license'] = 'Insert your license code or maybe your code is not valid';
$_['entry_notification_message'] = 'Notification message to client';
$_['entry_client_message'] = 'Message to the client about his request';
$_['entry_admin_message'] = 'Message to admin about a new request';
$_['avail_buttom'] = 'Change  buy button to notify button';

$_['tab_active'] = 'Active';
$_['tab_closed'] = 'Closed';
$_['button_delete'] = 'Delete';
$_['button_submit'] = 'Send';
$_['button_send'] = 'Notify';
$_['text_time'] = 'Time';
$_['text_product'] = 'Product';
$_['text_price'] = 'Price';
$_['text_mail'] = 'E-mail';
$_['text_name'] = 'Name';
$_['text_comment'] = 'Comment';
$_['text_statuse'] = 'Status';
$_['text_quantity'] = 'Quantity in stock';
$_['text_list'] = 'Notification list';
$_['text_sender'] = 'Product in the store!';
$_['text_edit_product'] = 'Notify on edit product';
$_['email_subject'] = 'Notice of availability!';
$_['text_mail_send'] = 'We are glad to inform you of a product that you\'re interested, are in the stock now!';
$_['text_link_page'] = 'Product page';
$_['text_get_availabilitylist'] = 'Go to the list of notifications';
$_['text_mail_on'] = 'Email where to send notifications of new requests';
$_['text_status_notprocessed'] = 'Availability is not processed';
$_['text_status_processed'] = 'Availability is processed';
$_['reload'] = 'Update data';
$_['success'] = 'Your notifications are send!';
$_['error'] = 'Nothing to send!';
$_['text_cron'] = 'Cron link';
$_['shortcodes'] = 'Shortcodes for messages';
$_['text_shortcode_name'] = '%name% = Client Name';
$_['text_shortcode_product_name'] = '%product_name% = Product Name'; 
$_['text_shortcode_price'] = '%price% = Price';
$_['text_shortcode_link'] = '%link% = Product link';
$_['text_shortcode_option_type'] = '%option_type% = Option Type';
$_['text_shortcode_option_name'] = '%option_name% = Option Name';
$_['hint'] = 'Hint!';
$_['text_button_cart_productpage'] = 'Id or class of cart button on product page';
$_['text_button_cart_other'] = 'Id or class of cart button on other pages';

$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_permission'] = 'Warning: You do not have permission to modify availability module!';
$_['error_email'] = 'E-Mail Address does not appear to be valid!';

?>