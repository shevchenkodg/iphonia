<?php
// Heading
$_['heading_title'] = 'Комментарий';
$_['heading_bossblog'] = 'Блог босса';

// Текст
$_['text_success'] = 'Успех: вы изменили отзывы!';

// Столбец
$_['column_article'] = 'Статья';
$_['column_author'] = 'Ваше имя';
$_['column_email'] = 'Электронная почта';
$_['column_comment'] = 'Комментарий';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата добавления';
$_['column_action'] = 'Действие';


$_['text_boss_catalog'] = 'Блог босса';
$_['text_boss_category'] = 'Категории блога';
$_['text_boss_articles'] = 'Статьи';
$_['text_boss_comments'] = 'Комментарии';
$_['text_boss_settings'] = 'Настройки';

// Запись
$_['entry_article'] = 'Статья: <br/> <span class = "help"> (Автозаполнение) </ span>';
$_['entry_author'] = 'Ваше имя:';
$_['entry_email'] = 'Электронная почта:';
$_['entry_comment'] = 'Комментарий:';
$_['entry_status'] = 'Состояние:';

// Ошибка
$_['error_permission'] = 'Внимание: у вас нет разрешения на изменение комментариев!';
$_['error_article'] = 'Требуется!';
$_['error_author'] = 'Автор должен быть от 3 до 64 символов!';
$_['error_text'] = 'Текст комментария должен содержать не менее 1 символа!';
$_['error_email'] = 'Email required!';
?>
