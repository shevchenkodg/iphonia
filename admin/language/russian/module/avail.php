<?php

$_['heading_title'] 			= 'Уведомить о наличии';
$_['text_module']	 			= 'Уведомление о наличии!';
$_['entry_name']       = 'Название модуля';

$_['text_success_avail']				= 'Настройки успешно изменены!';
$_['text_view_notices'] = 'View Notices';

$_['text_capcha'] = 'Capcha';


$_['entry_status']     = 'Cтатус:';
$_['entry_layout']        = 'Макет:';
$_['entry_position']      = 'Расположение:';
$_['entry_sort_order']    = 'Порядок сортировки:';
$_['text_content_top'] = 'Верх страницы';
$_['text_content_bottom'] = 'Низ страницы';
$_['text_column_left'] = 'Левая колонка';
$_['text_column_right'] = 'Правая колонка';
$_['text_license'] = 'Введите свой лицензионный ключ';
$_['text_button_cart_productpage'] = 'ID или class кнопки покупки товара на странице товара';
$_['text_button_cart_other'] = 'ID или class кнопки покупки товара на остальных страницах';
$_['text_options_status'] = 'Учитывать опции на товаре';
//Capcha

$_['entry_capcha_status'] = 'Captcha Status';
$_['entry_license'] = 'Введите лицензионный ключ или возможно вы ввели не коректный ключ';
$_['entry_notification_message'] = 'Сообщение клиенту о наличии товара';
$_['entry_client_message'] = 'Сообщение клиенту об оставленной заявке';
$_['entry_admin_message'] = 'Сообщение администратору о новой заявке';
$_['avail_buttom'] = 'Заменять кнопку купить на уведомить';

$_['tab_active'] = 'Активные';
$_['tab_closed'] = 'Обработанные';
$_['button_delete'] = 'Удалить';
$_['button_submit'] = 'Отправить';
$_['button_send'] = 'Уведомить';
$_['text_time'] = 'Время';
$_['text_product'] = 'Товар';
$_['text_price'] = 'Цена';
$_['text_mail'] = 'Телефон';
$_['text_name'] = 'Имя';
$_['text_comment'] = 'Комментарий';
$_['text_statuse'] = 'Статус';
$_['text_quantity'] = 'В наличии';
$_['text_list'] = 'Список уведомлений';
$_['text_sender'] = 'Товар в магазине!';
$_['text_edit_product'] = 'Уведомлять при сохранении настроек товара';
$_['email_subject'] = 'Уведомление о наличии товара!';
$_['text_mail_send'] = 'Мы рады сообщить вам о наличии товара который вас заинтересовал!';
$_['text_link_page'] = 'Страница товара';
$_['text_get_availabilitylist'] = 'Список заказов';
$_['text_mail_on'] = 'Email куда будет приходить информация о поступлении новой "заявках на уведомление".';
$_['text_status_notprocessed'] = 'Не выполнено';
$_['text_status_processed'] = 'Выполнено';
$_['reload'] = 'Обновить';
$_['success'] = 'Ваши уведомления отправлены!';
$_['error'] = 'Нет заявок для отправки';
$_['text_cron'] = 'Ссылка для cron\'а';
$_['text_shortcode_name'] = '%name% = Имя покупателя';
$_['text_shortcode_product_name'] = '%product_name% = Название товара'; 
$_['text_shortcode_price'] = '%price% = Стоимость';
$_['text_shortcode_link'] = '%link% = Ссылка на товар';
$_['text_shortcode_option_type'] = '%option_type% = Option Type';
$_['text_shortcode_option_name'] = '%option_name% = Название и значение опции(выводиться в строку)';
$_['shortcodes'] = 'Переменные которые можно использовать при создании писем';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_permission'] = 'Ошибка! Вы не имеете прав для изменения модуля';
$_['error_email'] = 'Телефон введен неверно';
$_['error_license'] = 'Ваш лицензионный ключ не верный!';
?>