<?php
    class ControllerModuleIwstdl extends Controller {

       private $error = array();

       public function index() {
          $this->language->load('module/iwstdl');


          $this->document->setTitle($this->language->get('heading_title'));

          $this->load->model('setting/setting');

          if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
             $this->model_setting_setting->editSetting('iwstdl', $this->request->post);

             $this->cache->delete('product');

             $this->session->data['success'] = $this->language->get('text_success');

             $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
          }

          $this->data['heading_title'] = $this->language->get('heading_title');


          $this->data['text_todo'] = $this->language->get('text_todo');

          $this->data['button_nazad'] = $this->language->get('button_nazad');

          if (isset($this->error['warning'])) {
             $this->data['error_warning'] = $this->error['warning'];
          } else {
             $this->data['error_warning'] = '';
          }

          if (isset($this->error['image'])) {
             $this->data['error_image'] = $this->error['image'];
          } else {
             $this->data['error_image'] = array();
          }

          $this->data['breadcrumbs'] = array();

          $this->data['breadcrumbs'][] = array(
              'text' => $this->language->get('text_home'),
              'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
              'separator' => false
          );

          $this->data['breadcrumbs'][] = array(
              'text' => $this->language->get('text_module'),
              'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
              'separator' => ' :: '
          );

          $this->data['breadcrumbs'][] = array(
              'text' => $this->language->get('heading_title'),
              'href' => $this->url->link('module/iwstdl', 'token=' . $this->session->data['token'], 'SSL'),
              'separator' => ' :: '
          );

          $this->data['action'] = $this->url->link('module/iwstdl', 'token=' . $this->session->data['token'], 'SSL');
          $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
          $this->data['modules'] = array();
          $this->load->model('design/layout');
          $this->data['layouts'] = $this->model_design_layout->getLayouts();
          $this->template = 'module/iwstdl.tpl';
          $this->children = array(
              'common/header',
              'common/footer'
          );

          $this->response->setOutput($this->render());
       }

       protected function validate() {
          if (!$this->user->hasPermission('modify', 'module/iwstdl')) {
             $this->error['warning'] = $this->language->get('error_permission');
          }

          if (isset($this->request->post['iwstdl_module'])) {
             foreach ($this->request->post['iwstdl_module'] as $key => $value) {
                if (!$value['todo']) {
                   $this->error['image'][$key] = $this->language->get('error_image');
                }
             }
          }

          if (!$this->error) {
             return true;
          } else {
             return false;
          }
       }

    }

?>