<?php
class ControllerModuleColors extends Controller {
	private $error = array();  
 
	public function index() {
		$this->language->load('module/colors');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/color');
		$this->load->model('setting/setting');
			
		$this->document->addScript('/admin/view/javascript/jquery/jquery-minicolors-master/jquery.minicolors.js');
		$this->document->addStyle('/admin/view/javascript/jquery/jquery-minicolors-master/jquery.minicolors.css');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_color->editColorOptions($this->request->post);
			$this->model_setting_setting->editSetting('color_kit', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('module/colors', 'token=' . $this->session->data['token'] , 'SSL'));
		}

		$this->getForm();
	}

	

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_name'] = $this->language->get('entry_name');
		
		$this->data['text_entry_name'] = $this->language->get('text_entry_name');
		$this->data['text_entry_color'] = $this->language->get('text_entry_color');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_edit_kits'] = $this->language->get('button_edit_kits');

		$text_array = array('text_settings','text_status_visibility','text_title','text_enable_popup','text_limit_colors','text_limit_colors_popup','text_size','text_width','text_height','text_ico_size','text_color_templates');
  		foreach($text_array as $txt){
			$this->data[$txt] =  $this->language->get($txt);
		}
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/colors', 'token=' . $this->session->data['token'] , 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->data['action'] = $this->url->link('module/colors', 'token=' . $this->session->data['token'] , 'SSL');
		$this->data['color_kits'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] , 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'] , 'SSL');
				
		$this->data['color_options'] = array();			
		if (isset($this->request->post['color_options'])) {
			$this->data['color_options'] = $this->request->post['color_options'];
		} else {
			$this->data['color_options'] = $this->model_catalog_color->getColorOptionDescriptions();
		}	
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$colors_cfg = $this->config->get('color_kit');
		$colors_array = array ('name', 'limit','width','title','height','ico_width','ico_height','visible','enable_popup');
		
		foreach ($colors_array as $datas) {
			if (isset($this->request->post['color_kit'][$datas])) {
				$this->data[$datas] = $this->request->post['color_kit'][$datas];
			} else {			
				$this->data[$datas] = $colors_cfg[$datas];
			}
		}
		
		$this->template = 'module/colors.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'module/colors')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$arr = $this->request->post['option_value'];
		
		foreach($arr as $key => $opt){
			foreach($arr[$key]['r_opt_description'] as $value){			
				if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 64)) {
					$this->error['warning'] = $this->language->get('error_name');
				}		
			}
			
			if (utf8_strlen($opt['color']) < 4) {
				$this->error['warning'] = $this->language->get('error_color');
			}				
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function install() {
		$this->load->model('catalog/color');
		$this->model_catalog_color->createDatabaseTables();
	}

	public function uninstall() {

		$this->load->model('catalog/color');
		$this->model_catalog_color->dropDatabaseTables();
	}
}
?>