<?php
class ControllerCatalogColorkits extends Controller {
	private $error = array();  
 
	public function index() {
		$this->language->load('catalog/color_kits');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/colorkit');
		
		$this->getList();
	}
	
	public function insert() {
		$this->language->load('catalog/color_kits');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/colorkit');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_colorkit->addColorKit($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {	
		$this->language->load('catalog/color_kits');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/colorkit');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_colorkit->editColorKit($this->request->get['color_kit_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function delete() {
		$this->language->load('catalog/color_kits');

		$this->document->setTitle($this->language->get('heading_title'));
 		
		$this->load->model('catalog/colorkit');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $color_kit_id) {
				$this->model_catalog_colorkit->deleteColorKit($color_kit_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}
	
	protected function getList() {

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'ck.name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['kits_auto'] = $this->url->link('catalog/color_kits/auto', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['insert'] = $this->url->link('catalog/color_kits/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/color_kits/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['color_list'] = $this->url->link('module/colors', 'token=' . $this->session->data['token'] , 'SSL');
		 
		$this->data['filters'] = array();
		
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$filter_total = $this->model_catalog_colorkit->getTotalColorKits();
		
		$results = $this->model_catalog_colorkit->getColorKitsGroups($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/color_kits/update', 'token=' . $this->session->data['token'] . '&color_kit_id=' . $result['color_kit_id'] . $url, 'SSL')
			);

			$this->data['color_kits'][] = array(
				'color_kit_id'	  => $result['color_kit_id'],
				'name'            => $result['name'],
				'status'          => $result['status'],
				'tpl'          	  => $this->language->get('tpl_'.$result['tpl']),
				'action'          => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_kits'] = $this->language->get('column_kits');
		$this->data['column_tpl'] = $this->language->get('column_tpl');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_color_list'] = $this->language->get('button_color_list');
		$this->data['button_auto'] = $this->language->get('button_auto');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . '&sort=ck.name' . $url, 'SSL');
		$this->data['sort_tpl'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . '&sort=ck.tpl' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . '&sort=ck.status' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $filter_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/color_kits.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {	
	
		$this->document->addStyle('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
		$this->document->addStyle('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css');
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_image_manager'] = 'Image manager';
		$this->data['no_image'] = '/image/no_image.jpg';
		
		$this->data['entry_group'] = $this->language->get('entry_group');		
		$this->data['column_kits'] = $this->language->get('column_kits');
		$this->data['column_status'] = $this->language->get('column_status');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_remove'] = $this->language->get('button_remove');

		$langs = array('entry_name_product','name_product','name_color','status_on','status_off','template','template_colors','template_photos','column_photo','choose','clear','');
		foreach($langs as $lang){
			$this->data[$lang] = $this->language->get($lang);
		}
		
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
						
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

	
		if (!isset($this->request->get['color_kit_id'])) {
			$this->data['action'] = $this->url->link('catalog/color_kits/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else { 
			$this->data['action'] = $this->url->link('catalog/color_kits/update', 'token=' . $this->session->data['token'] . '&color_kit_id=' . $this->request->get['color_kit_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL');
	
		$this->data['token'] = $this->session->data['token'];
						
		$this->data['get_colors'] = $this->model_catalog_colorkit->getColors();				
		if (isset($this->request->post['color_kits'])) {
			$this->data['color_kits'] = $this->request->post['color_kits'];
		} elseif (isset($this->request->get['color_kit_id'])) {
			$results = $this->model_catalog_colorkit->getColorKit($this->request->get['color_kit_id']);
			
			$this->data['color_kit'] = $this->model_catalog_colorkit->getColorKitDescription($this->request->get['color_kit_id']);
			$this->load->model('catalog/product');
			
			
			$this->data['color_kits'] = array();
			foreach($results as $result){
				$product_info = $this->model_catalog_product->getProduct($result['product_id']);
				if ($product_info) {
					$this->data['color_kits'][] = array(
						'product_id' => $product_info['product_id'],
						'product_name' => $product_info['name'],
						'name' => $result['name'],
						'image' => $result['image'],
						'option_id' => $result['option_id'],
						'color' => $result['color']
					);
				}
			}

			
		} else {
			$this->data['color_kits'] = array();
			$this->data['color_kit']['name'] = '';
			$this->data['color_kit']['name'] = 'color';
			$this->data['color_kit']['status'] = '1';
		}

		$this->template = 'catalog/color_kits_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/color_kits')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 255)) {
				$this->error['warning'] = $this->language->get('error_name');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/color_kits')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}	
		
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_sku'])) {
			$this->load->model('catalog/colorkit');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}
			
			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}
				
			if (isset($this->request->get['filter_sku'])) {
				$filter_sku = $this->request->get['filter_sku'];
			} else {
				$filter_sku = '';
			}
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'filter_sku' => $filter_sku,
				'start'        => 0,
				'limit'        => $limit
			);
				
			$results = $this->model_catalog_colorkit->getProducts($data);
			
			foreach ($results as $result) {								
				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),	
					'model'      => $result['model'],
					'sku'        => $result['sku']
				);	
			}

		}

		$this->response->setOutput(json_encode($json));
	}
	
	public function auto(){
		$url = '';
		$this->language->load('catalog/color_kits');
		
		$this->data['heading_title'] = $this->language->get('head_auto');

		$this->document->setTitle($this->language->get('head_auto'));
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('head_auto'),
			'href'      => $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->language->load('catalog/color_kits');

		$this->document->setTitle($this->language->get('head_auto'));
		
		$this->load->model('catalog/colorkit');
		
		$color_list = $this->model_catalog_colorkit->getColors();
		
		$products = $this->model_catalog_colorkit->getProductWidthColors($color_list);
		
		$this->data['kits_auto'] = $this->url->link('catalog/color_kits/auto', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['insert'] = $this->url->link('catalog/color_kits', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['color_list'] = $this->url->link('module/colors', 'token=' . $this->session->data['token'] , 'SSL');
		$this->data['action'] = $this->url->link('catalog/color_kits/auto', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST'){
			$status = (isset($this->request->post['status'])) ? $this->request->post['status'] : '0';
			$save = $this->model_catalog_colorkit->autoFillKits($this->request->post,$status);
			$this->data['success'] = $this->language->get('successful');
		}	
		

		$langs = array('button_sets','button_auto','button_color_list','column_status','text_found','button_add_auto');
		foreach($langs as $lang){
			$this->data[$lang] = $this->language->get($lang);
		}
		$this->data['products'] = $products;

		$this->template = 'catalog/color_kits_auto.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
}	
	