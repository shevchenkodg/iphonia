<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
	<div id="notification">
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
	
	</div>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            
            <div id="tabs" class="htabs">
                <a href="#tab-general"><?php echo $tab_general; ?></a>
				<a href="#tab-change"><?php echo $tab_change; ?></a>
                <a href="#tab-license"><?php echo $tab_license; ?></a>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="tab-general">
                    <table class="form">
                        <tr>
                            <td><?php echo $entry_status_name_admin;?></td>
                            <td><input size="100" maxlength="255" name="archive_product_status_name_admin" value="<?php echo $archive_product_status_name_admin;?>"/>
                                <?php if (isset($error_text_status_name_admin)) { ?>
                  <span class="error"><?php echo $error_text_status_name_admin; ?></span>
                  <?php } ?>
                            </td>
                        </tr>
						 <tr>
                                <td><?php echo $entry_max_most_viewed;?></td>
                                <td><input name="archive_product_max_viewed" value="<?php echo $archive_product_max_viewed;?>"/></td>
                            </tr>
							<tr>
                                <td><?php echo $entry_most_viewed;?></td>
                                <td>
									<select name="archive_product_most_viewed">	
										 <?php if ($archive_product_most_viewed) { ?>
											<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
											<option value="0"><?php echo $text_disabled; ?></option>
										<?php } else { ?>
											<option value="1"><?php echo $text_enabled; ?></option>
											<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
										<?php } ?>										
									</select>
								</td>
                            </tr>
                    </table>
                
                    <div id="languages" class="htabs">
                        <?php foreach ($languages as $language) { ?>
                            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
                        <?php } ?>
                    </div>
                     <?php foreach ($languages as $language) { ?>
                    <div id="language<?php echo $language['language_id']; ?>">
                        <table class="form">
                            <tr>
                                <td><?php echo $entry_text_catalog_product;?></td>
                                <td><textarea id="text_catalog<?php echo $language['language_id']; ?>" name="archive_product_text_catalog_product[<?php echo $language['language_id']; ?>]" ><?php echo (isset($archive_product_text_catalog_product[$language['language_id']]))?$archive_product_text_catalog_product[$language['language_id']]:'';?></textarea>
                                 <?php if (isset($error_text_catalog_product[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_text_catalog_product[$language['language_id']]; ?></span>
                  <?php } ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $entry_text_status_wishlist;?></td>
                                <td><input size="100" maxlength="255" name="archive_product_text_status_wishlist[<?php echo $language['language_id']; ?>]" value="<?php echo (isset($archive_product_text_status_wishlist[$language['language_id']]))?$archive_product_text_status_wishlist[$language['language_id']]:'';?>"/>
                                <?php if (isset($error_text_status_wishlist[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_text_status_wishlist[$language['language_id']]; ?></span>
                  <?php } ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $entry_header_replaced;?></td>
                                <td><input size="100" maxlength="255" name="archive_product_header_replaced[<?php echo $language['language_id']; ?>]" value="<?php echo (isset($archive_product_header_replaced[$language['language_id']]))?$archive_product_header_replaced[$language['language_id']]:'';?>"/></td>
                            </tr>
                        </table>
                    </div>
                     <?php }?>
                </div>
				<div id="tab-change">					
					<div id="tabs_apply_arhive" class="htabs">
						<a href="#tab-apply"><?php echo $tab_change_apply_arhive; ?></a>
						<a href="#tab-unapply"><?php echo $tab_change_unapply_arhive; ?></a>							
					</div>
					<div id="tab-apply">
						<table class="form">
							<tr>
								<td><?php echo $entry_status; ?></td>
								<td>
									<select name="archive_product_status">							  
										<option value="-1" selected="selected"><?php echo $text_none; ?></option>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
									</select>
								</td>
							</tr>
							<tr>
								<td><?php echo $entry_minimum;?></td>
								<td><input name="archive_product_minimum" value="<?php echo $archive_product_minimum;?>"/></td>
							</tr>
							 <tr>
								<td><?php echo $entry_date_modified ;?></td>
								<td><input name="archive_product_date_modified" value="<?php echo $archive_date_modified;?>" class="date"/></td>
							</tr>
							<tr>
								<td><?php echo $entry_list_products; ?></td>
								<td><input type="text" name="archive_product_list_product" value="" style="width:450px;"/></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<div id="archive_product_list_products" class="scrollbox" style="width:450px;">
										<?php $class = 'odd'; ?>
										<?php foreach ($archive_product_list_products as $product) { ?>
											<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
											<div id="archive_product_list_products<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"> 
												<?php echo $product['name']; ?>
												<img src="view/image/delete.png" />
												<input type="hidden" name="archive_product_list_products[]" value="<?php echo $product['product_id']; ?>" />
											</div>
										<?php } ?>
									</div>
								</td>
							</tr>
						</table>
						<div class="buttons">
							<a onclick="applyArhive();" class="button"><?php echo $button_apply_arhive; ?></a>							
						</div>
					</div>
					<div id="tab-unapply">
						<table class="form">
							<tr>
								<td><?php echo $entry_minimum_apply;?></td>
								<td><input name="archive_product_minimum_apply" value="<?php echo $archive_product_minimum_apply;?>"/></td>
							</tr>
							 <tr>
								<td><?php echo $entry_date_modified_apply ;?></td>
								<td><input name="archive_product_date_modified_apply" value="<?php echo $archive_date_modified_apply;?>" class="date"/></td>
							</tr>
							<tr>
								<td><?php echo $entry_list_products_apply; ?></td>
								<td><input type="text" name="archive_product_list_product_apply" value="" style="width:450px;"/></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<div id="archive_product_list_products_apply" class="scrollbox" style="width:450px;">
										<?php $class = 'odd'; ?>
										<?php foreach ($archive_product_list_products_apply as $product) { ?>
											<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
											<div id="archive_product_list_products_apply<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"> 
												<?php echo $product['name']; ?>
												<img src="view/image/delete.png" />
												<input type="hidden" name="archive_product_list_products_apply[]" value="<?php echo $product['product_id']; ?>" />
											</div>
										<?php } ?>
									</div>
								</td>
							</tr>
							<tr>
								<td><?php echo $entry_status_apply; ?></td>
								<td>
									<select name="archive_product_status_apply">							  
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
									</select>
								</td>
							</tr>
						</table>
						<div class="buttons">
							<a onclick="unapplyArhive();" class="button"><?php echo $button_unapply_arhive; ?></a>							
						</div>
					</div>
						
				</div>                    
                <div id="tab-license">
                    <table class="form">
                   <tr>
                       <td>Название:</td>
                       <td><?php echo $heading_title; ?></td>
                   </tr>
                   <tr>
                       <td>Версия:</td>
                       <td>1.1.0</td>
                   </tr>
                   <tr>
                       <td>Разработка:</td>
                       <td><a href="http://wemake.org.ua/" target="_blank">WeMake</a></td>
                   </tr>
                   <tr>
                       <td>Лицензия:</td>
                       <td><?php if($license){?>
                           <span style="color:green;">Получена</span>
                       <?php }else{?>
                           <span style="color:red;"><?php echo $license_text; ?></span>
                       <?php }?>
                           </td>
                   </tr>
			<tr>
                       <td>Как получить лицензию:</td>
                       <td>Для получения лицензии вам необходимо связаться с разработчиками удобным для вас способом. В обращении вы должны указать на каком сайте, когда и каким пользователем была осуществлена покупка модуля и имя  домена, на котором будет работать модуль. <br />Контакты для получения лицензии: 
Skype: AlenGoncharuk или smile-av, 
e-mail: alensg@yandex.ru или smile-av@yandex.ru</td>

                   </tr>
               </table>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
	<?php foreach ($languages as $language) { ?>
		CKEDITOR.replace('text_catalog<?php echo $language['language_id']; ?>', {
			filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
			filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
		});
	<?php } ?>

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});

	$('#tabs a').tabs();
    $('#languages a').tabs();
	$('#tabs_apply_arhive a').tabs();
	
	function applyArhive(){	
		$.ajax({
			url: 'index.php?route=module/archive_product/applyarhive&token=<?php echo $token; ?>',
			type: 'post',
			data: $('#form').serialize(),
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, .information, .error').remove();
				
				if (json['redirect']) {
					location = json['redirect'];
				}
				
				if (json['success']) {
					$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.success').fadeIn('slow');
				}	
				if (json['error']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.warning').fadeIn('slow');
				}
			}
		});
	
		return false;
	}
	
	function unapplyArhive(){	
		$.ajax({
			url: 'index.php?route=module/archive_product/unapplyarhive&token=<?php echo $token; ?>',
			type: 'post',
			data: $('#form').serialize(),
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, .information, .error').remove();
				
				if (json['redirect']) {
					location = json['redirect'];
				}
				
				if (json['success']) {
					$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.success').fadeIn('slow');
				}	
				if (json['error']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.warning').fadeIn('slow');
				}
			}
		});
	
		return false;
	}


	$('input[name=\'archive_product_list_product\']').autocomplete({
		delay: 0,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {		
					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.product_id
						}
					}));
				}
			});		
		}, 
		select: function(event, ui) {
			$('#archive_product_list_products' + ui.item.value).remove();		
			$('#archive_product_list_products').append('<div id="archive_product_list_products' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" name="archive_product_list_products[]" value="' + ui.item.value + '" /></div>');
			$('#archive_product_list_products div:odd').attr('class', 'odd');
			$('#archive_product_list_products div:even').attr('class', 'even');				
			return false;
		},
		focus: function(event, ui) {
		  return false;
		}
	});

	$('#archive_product_list_products div img').live('click', function() {
		$(this).parent().remove();	
		$('#archive_product_list_products div:odd').attr('class', 'odd');
		$('#archive_product_list_products div:even').attr('class', 'even');	
	});
	
	$('input[name=\'archive_product_list_product_apply\']').autocomplete({
		delay: 0,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {		
					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.product_id
						}
					}));
				}
			});		
		}, 
		select: function(event, ui) {
			$('#archive_product_list_products_apply' + ui.item.value).remove();		
			$('#archive_product_list_products_apply').append('<div id="archive_product_list_products_apply' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" name="archive_product_list_products_apply[]" value="' + ui.item.value + '" /></div>');
			$('#archive_product_list_products_apply div:odd').attr('class', 'odd');
			$('#archive_product_list_products_apply div:even').attr('class', 'even');				
			return false;
		},
		focus: function(event, ui) {
		  return false;
		}
	});

	$('#archive_product_list_products_apply div img').live('click', function() {
		$(this).parent().remove();	
		$('#archive_product_list_products_apply div:odd').attr('class', 'odd');
		$('#archive_product_list_products_apply div:even').attr('class', 'even');	
	});
//--></script> 
<?php echo $footer; ?>
