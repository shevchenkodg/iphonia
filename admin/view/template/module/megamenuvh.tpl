<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>

  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons" id="buttons-apply">
		<a onclick="apply_btn()" class="btn btn-success letter_save"><span><?php echo $text_btn_apply;?></span></a>
		<a onclick="$('#form-megamenuvh').submit();" class="btn btn-primary letter_primary"><span><?php echo $button_save; ?></span></a>
		<a href="<?php echo $cancel; ?>" class="btn btn-default" title="<?php echo $button_cancel; ?>"><i class="fa fa-reply"></i></a>
	  </div>
    </div>
<script type="text/javascript">
var loading_img_qvp = '<img src="view/javascript/megamenu-assets/loading_quickview_pro.svg" />';
function creatOverlayLoadPage(action) {
	if (action) {
		$('#loading_megamenu').html(loading_img_qvp);
		$('#loading_megamenu').show();
	} else {
		$('#loading_megamenu').html('');
		$('#loading_megamenu').hide();
	}
}
	$(window).scroll(function(){
		if ($(window).scrollTop() > 100){
			$("#buttons-apply").addClass("fixed-btn-top");
		} else {
			$("#buttons-apply").removeClass("fixed-btn-top");
		}
	});
function apply_btn(){
$('body').prepend('<div id="loading_megamenu"></div>');
$('#content').addClass('mfp-bg-megamenu');
creatOverlayLoadPage(true); 
	$(".success").remove();
	$.post($("#form-megamenuvh").attr('action'), $("#form-megamenuvh").serialize(), function(html) {
			var $success = $(html).find(".success, .warning");
			if ($success.length > 0) {
				$(".box").before($success);
				creatOverlayLoadPage(false); 
				$('#content').removeClass('mfp-bg-megamenu');
				$('#loading_megamenu').hide();
				
			}
		});
	}
</script> 
    <div class="content">
	<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="view/javascript/megamenu-assets/autocompletemegameu.js"></script>
	<link href="view/javascript/megamenu-assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen" />
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-megamenuvh">
		<div>

        <table class="form">
			<tr>
				<td colspan="2" class="title-setting"><?php echo $tab_menu_general;?></td>
			</tr>
			<tr>
				<td><?php echo $entry_megamenu_status;?></td>
				<td>
					<select name="config_megamenu_status" class="form-control">
						<?php if ($config_megamenu_status) { ?>
							<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
							<option value="1"><?php echo $text_enabled; ?></option>
							<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>					
					</select>
				</td>
			</tr>
			<tr>
				<td><?php echo $entry_menu_selection;?></td>
				<td>
					<select onchange="change_menu_theme(this.value);" name="config_main_menu_selection" id="input-menu-selection" class="form-control">
						<?php if ($config_main_menu_selection =='1') { ?>
							<option value="1" selected="selected"><?php echo $text_main_vertical_menu; ?></option>
							<option value="0"><?php echo $text_main_horizontal_menu; ?></option>
						<?php } else{ ?>
							<option value="0" selected="selected"><?php echo $text_main_horizontal_menu; ?></option>
							<option value="1"><?php echo $text_main_vertical_menu; ?></option>                  
						<?php } ?>					
					</select>
				</td>
			</tr>
			<script>
			$(window).load(function(){ 
				var config_main_menu_selection = $('#input-menu-selection option:selected').val();	
				if (config_main_menu_selection =='1') {
					//$('.menu_always_open').toggleClass('active');
					$('.additional_menu').toggleClass('active');
					
					$('.fixed-panel-top').removeClass('active');
				} else if (config_main_menu_selection =='0') {
					//$('.menu_always_open').removeClass('active');
					$('.additional_menu').removeClass('active');
					
					$('.fixed-panel-top').toggleClass('active');
				}
			})
			function change_menu_theme(config_main_menu_selection){						
				if (config_main_menu_selection =='1') {
					//$('.menu_always_open').addClass('active');
					$('.additional_menu').addClass('active');
					
					$('.fixed-panel-top').removeClass('active');
				} else if (config_main_menu_selection =='0') {
					//$('.menu_always_open').removeClass('active');
					$('.additional_menu').removeClass('active');
					
					$('.fixed-panel-top').toggleClass('active');
				}
			}
			</script>
			<tr class="fixed-panel-top">
				<td><?php echo $entry_fixed_panel_top;?></td>
				<td>
					<select name="config_fixed_panel_top" id="input-fixed-panel-top" class="form-control">
						<?php if ($config_fixed_panel_top) { ?>
							<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
							<option value="1"><?php echo $text_enabled; ?></option>
							<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr  class="horizontal_menu" >
				<td><?php echo $text_main_horizontal_menu;?></td>
				<td>
					<select name="horizontal_menu_width_setting" id="input-horizontal-menu-width-setting" class="form-control">
						<?php if ($horizontal_menu_width_setting) { ?>
							<option value="1" selected="selected"><?php echo $text_full_screen_menu; ?></option>
							<option value="0"><?php echo $text_full_screen_layout ?></option>
						<?php } else { ?>
							<option value="1"><?php echo $text_full_screen_menu; ?></option>
							<option value="0" selected="selected"><?php echo $text_full_screen_layout; ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr class="menu_always_open">
				<td><?php echo $text_home_page;?></td>
				<td>
				  <?php if ($config_menu_always_open_on_the_left) { ?>
				  <input type="radio" name="config_menu_always_open_on_the_left" value="1" checked="checked" />
				  <?php echo $text_yes; ?>
				  <input type="radio" name="config_menu_always_open_on_the_left" value="0" />
				  <?php echo $text_no; ?>
				  <?php } else { ?>
				  <input type="radio" name="config_menu_always_open_on_the_left" value="1" />
				  <?php echo $text_yes; ?>
				  <input type="radio" name="config_menu_always_open_on_the_left" value="0" checked="checked" />
				  <?php echo $text_no; ?>
				  <?php } ?>
				</td>
			</tr>
			<tr class="form-group">
				<td colspan="2" class="title-setting"><?php echo $ns_text_setting_general_menu;?></td>
			</tr>
			<tr class="form-group">
				<td><i class="fa fa-exclamation-triangle required_cat"></i>  <?php echo $text_category_add_auto_description;?></td>
				<td class="title-setting"><a class="btn btn-success letter_save" id="autocategoryadd"><span><?php echo $text_category_add_auto;?></span></a></td>
			</tr>
			<script type="text/javascript">
$('#autocategoryadd').bind('click',function() {
$('body').prepend('<div id="loading_megamenu"></div>');
$('#content').addClass('mfp-bg-megamenu');
creatOverlayLoadPage(true); 
				var license_key_deactivation =1;
				var success = 'false';				
					$.ajax({
						type:'get',
						dataType:'json',
						data:'license_key_deactivation=' + license_key_deactivation,
						url:'index.php?route=module/megamenuvh/autocategoryadd&token=<?php echo $token; ?>',							
						success: function(json){
							if (json['success']) {
							creatOverlayLoadPage(false); 
							$('#content').removeClass('mfp-bg-megamenu');
							$('#loading_megamenu').hide();
								location.reload(); 
							}	
						}
					});				
			
			});
			</script>
        </table>
		</div>
		<div>
			<?php $menu_item_row = 0; ?>
			<div class="vtabs">
				<?php foreach ($config_menu_items as $config_menu_item) { ?>				
					<a id="menu-item-<?php echo $menu_item_row; ?>" class="menu-tab-link" href="#tab-menu-item-<?php echo $menu_item_row; ?>" >
					<?php  echo $config_menu_item['namemenu'][$lang_id]; ?>
					<i class="fa fa-minus-circle remove_menu_item" onclick="$('#menu-item-<?php echo $menu_item_row; ?>').remove(); $('#tab-menu-item-<?php echo $menu_item_row; ?>').remove(); $('a[href=\'#tab-menu-item-0\']').trigger('click'); return false;"></i> 
					</a>
				<?php $menu_item_row++; ?>
				<?php } ?>
				 <span id="addMenuItem" onclick="addItemCategoryBanner();"><?php echo $text_add; ?>&nbsp;<i class="fa fa-plus-circle"></i></span> </div>
						
			</div>
			<?php $menu_item_row = 0; ?>
			<div id="ns-right-block-menu" class="vtabs-content">
				<div class="col-lg-9 col-md-8 megamenu-content">
					<?php foreach ($config_menu_items as $config_menu_item) { ?>
						<div id="tab-menu-item-<?php echo $menu_item_row; ?>">		
							<ul class="general-<?php echo $menu_item_row; ?> htabs list-unstyled">
								<li class="active"><a href="#tab-menu-setting-<?php echo $menu_item_row; ?>" data-toggle="tab"><?php echo $ns_tab_menu_setting ?></a></li> 
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_category_<?php echo $menu_item_row; ?>"><a href="#tab-category-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li>
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_manufacturer_<?php echo $menu_item_row; ?>"><a href="#tab-manufacturer-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li> 								
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_information_<?php echo $menu_item_row; ?>"><a href="#tab-information-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li> 								
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_product_<?php echo $menu_item_row; ?>"><a href="#tab-product-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li> 								
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_link_<?php echo $menu_item_row; ?>"><a href="#tab-link-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li> 																
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_html_<?php echo $menu_item_row; ?>"><a href="#tab-html-<?php echo $menu_item_row; ?>_options" data-toggle="tab"><?php echo $ns_tab_html; ?></a></li> 
								<li  class="show_elements_<?php echo $menu_item_row; ?> show_elements_add_html_<?php echo $menu_item_row; ?>"><a href="#tab-add-<?php echo $menu_item_row; ?>-html" data-toggle="tab"><?php echo $ns_tab_add_html; ?></a></li> 
							</ul>
							<div class="tab-content">
								<div id="tab-menu-setting-<?php echo $menu_item_row; ?>">	
								<table class="form">
									<tr>
										<td><?php echo $ns_text_menu_name;?></td>
										<td>
											<?php foreach ($languages as $language) { ?>
												<div class="input-group pull-left">
													<span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
													<input id="namemenu_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>" onChange="change_name_menu('<?php echo $menu_item_row; ?>', '<?php echo $language['language_id']; ?>')" class="form-control" type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][namemenu][<?php echo $language['language_id']; ?>]" value="<?php echo $config_menu_item['namemenu'][$language['language_id']]; ?>" />
												</div>
											<?php } ?>
										</td>
									</tr>
									<tr class="additional_menu">
										<td><?php echo $ns_text_additional_menu; ?></td>
										<td>
											<select name="config_menu_item[<?php echo $menu_item_row; ?>][additional_menu]" id="input-status-additional-menu" class="form-control">
												<?php if ($config_menu_item['additional_menu'] =='additional') { ?>
													<option value="additional" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="left"><?php echo $text_disabled; ?></option>
												<?php } else { ?>
													<option value="additional"><?php echo $text_enabled; ?></option>
													<option value="left" selected="selected"><?php echo $text_disabled; ?></option>
												<?php } ?>
											</select>
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_menu_link;?></td>
										<td>
											<input class="form-control" type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][link]" value="<?php echo $config_menu_item['link']; ?>" />
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_type;?></td>
										<td>
											<select onChange="sel_type_category('<?php echo $menu_item_row; ?>')"  name="config_menu_item[<?php echo $menu_item_row; ?>][menu_type]" id="input-menu_type_<?php echo $menu_item_row; ?>" class="form-control">											
												<option value="category" <?php if($config_menu_item['menu_type']=="category") { ?> selected="selected" <?php } ?>><?php echo $ns_text_type_category; ?></option>
												<option value="html" <?php if($config_menu_item['menu_type']=="html") { ?> selected="selected" <?php } ?>><?php echo $ns_text_type_html; ?></option>
												<option value="manufacturer" <?php if($config_menu_item['menu_type']=="manufacturer") {?> selected="selected" <?php } ?>><?php echo $ns_text_type_manufacturer; ?></option>
												<option value="information" <?php if($config_menu_item['menu_type']=="information") {?> selected="selected" <?php } ?>><?php echo $ns_text_type_information; ?></option>
												<option value="product" <?php if($config_menu_item['menu_type']=="product") { ?> selected="selected" <?php } ?>><?php echo $ns_text_type_product; ?></option>											
												<option value="link" <?php if($config_menu_item['menu_type']=="link") { ?> selected="selected" <?php } ?>><?php echo $ns_text_type_link; ?></option>
											</select>
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_status; ?></td>
										<td>
											<select name="config_menu_item[<?php echo $menu_item_row; ?>][status]" id="input-status" class="form-control">
												<?php if ($config_menu_item['status']) { ?>
													<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
												<?php } else { ?>
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
												<?php } ?>
											</select>
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_sticker_parent; ?></td>
										<td>
											<select class="form-control" name="config_menu_item[<?php echo $menu_item_row; ?>][sticker_parent]">
												<option value="0"><?php echo $ns_text_select_sticker;?></option>					
												<option value="new" <?php if($config_menu_item['sticker_parent'] =='new'){?> selected="selected" <?php } ?>><?php echo "NEW";?></option>
												<option value="top" <?php if($config_menu_item['sticker_parent'] =='top'){?> selected="selected" <?php } ?>><?php echo "TOP";?></option>
												<option value="sale" <?php if($config_menu_item['sticker_parent'] =='sale'){?> selected="selected" <?php } ?>><?php echo "SALE";?></option>
											</select>
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_sort_menu;?></td>
										<td>
											<input class="form-control" type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][sort_menu]" value="<?php echo $config_menu_item['sort_menu']; ?>" />
										</td>
									</tr>	
									<tr>
									<td><?php echo $ns_text_thumb; ?></td>
									  <td><div class="image"><img src="<?php echo $config_menu_item['thumb']; ?>" alt="" id="thumb-image<?php echo $menu_item_row; ?>" />
										  <input type="hidden" name="config_menu_item[<?php echo $menu_item_row; ?>][image]" value="<?php echo $config_menu_item['image']; ?>" id="icon-image<?php echo $menu_item_row; ?>" />
										  <br />
										  <a onclick="image_upload('icon-image<?php echo $menu_item_row; ?>', 'thumb-image<?php echo $menu_item_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb-image<?php echo $menu_item_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#icon-image<?php echo $menu_item_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
									</tr>	
								</table>		
								</div>
									
								<div id="tab-category-<?php echo $menu_item_row; ?>_options">	
									<table class="form">								
									<tr>
										<td><?php echo $ns_type_dropdown_list; ?></td>
										<td>
											<select onChange="sel_type_category('<?php echo $menu_item_row; ?>')" name="config_menu_item[<?php echo $menu_item_row; ?>][variant_category]" id="input_variant_category_<?php echo $menu_item_row; ?>" class="form-control">
												<option value="simple" <?php if($config_menu_item['variant_category']=="simple") { ?> selected="selected" <?php } ?>><?php echo $ns_type_dropdown_simple; ?></option>
												<option value="full" <?php if($config_menu_item['variant_category']=="full") { ?> selected="selected" <?php } ?>><?php echo $ns_type_dropdown_full; ?></option>
												<option value="full_image" <?php if($config_menu_item['variant_category']=="full_image") { ?> selected="selected" <?php } ?>><?php echo $ns_type_dropdown_full_image; ?></option>
											</select>
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_show_sub_categories; ?></td>
										<td>
											<select id="input-category_show_subcategory" name="config_menu_item[<?php echo $menu_item_row; ?>][show_sub_category]" class="form-control">
												<?php if ($config_menu_item['show_sub_category']) { ?>
													<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
												<?php } else { ?>
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
												<?php } ?>
											</select>										
										</td>
									</tr>
									<tr>
										<td><?php echo $ns_text_product_width; ?></td>
										<td>
											<input type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][category_img_width]" value="<?=isset($config_menu_item['category_img_height'])?$config_menu_item['category_img_height']:50?>" placeholder="<?php echo $ns_text_product_width; ?>" class="form-control" />									 
										</td>
									</tr>		
									<tr>
										<td><?php echo $ns_text_product_height; ?></td>
										<td>
											<input type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][category_img_height]" value="<?=isset($config_menu_item['category_img_height'])?$config_menu_item['category_img_height']:50?>" placeholder="<?php echo $ns_text_product_height; ?>" class="form-control" />									 
										</td>
									</tr>									
									<tr>
										<td><?php echo $ns_text_category_list; ?></td>
										<td>
											<input type="text" name="category" value="" placeholder="<?php echo $ns_text_category; ?>" id="input-category-<?php echo $menu_item_row; ?>" class="form-control" />
											<div id="category-category-<?php echo $menu_item_row; ?>" class="well well-sm">
												<?php if(!empty($config_menu_item['category_list_sel'])) { ?>
													<?php foreach ($config_menu_item['category_list_sel'] as $category_list) { ?>
														<div class="row-category-menu" id="category-item-<?php echo $menu_item_row; ?>-<?php echo $category_list['category_id']; ?>">
															<i class="btn btn-danger fa fa-trash-o fa-lg"></i>
															<input class="form-control input-sort-category" size="3" type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][sort_category][<?php echo $category_list['category_id']; ?>]" value="<?php echo $sort_category[$menu_item_row][$category_list['category_id']]?>"/>
															<select class="input-sticker-category form-control" name="config_menu_item[<?php echo $menu_item_row; ?>][sticker][<?php echo $category_list['category_id']; ?>]">
																<option value="0"><?php echo $ns_text_select_sticker;?></option>					
																<option value="new" <?php if(isset($sticker_list_new[$menu_item_row][$category_list['category_id']])){?> selected="selected" <?php } ?>><?php echo "NEW";?></option>
																<option value="top" <?php if(isset($sticker_list_top[$menu_item_row][$category_list['category_id']])){?> selected="selected" <?php } ?>><?php echo "TOP";?></option>
																<option value="sale" <?php if(isset($sticker_list_sale[$menu_item_row][$category_list['category_id']])){?> selected="selected" <?php } ?>><?php echo "SALE";?></option>
															</select>
															<?php echo $category_list['name']; ?>
															<input type="hidden" name="config_menu_item[<?php echo $menu_item_row; ?>][category_list][]" value="<?php echo $category_list['category_id']; ?>" />
														</div>
													<?php } ?>	
												<?php } ?>	
											</div>
										</td>
									</tr>
									</table>		
								</div>
									
								<div id="tab-html-<?php echo $menu_item_row; ?>_options">
									<div class="tab-html-<?php echo $menu_item_row; ?> htabs" id="language_html_<?php echo $menu_item_row; ?>">
										<?php foreach ($languages as $language) { ?>
											<a href="#language_html_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
										<?php } ?>
									</div>
									<?php foreach ($languages as $language) { ?>
										<div id="language_html_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>">
											<table class="form">
												<tr>
													<td colspan="2">
														<textarea id="html_description_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>" name="config_menu_item[<?php echo $menu_item_row; ?>][html_block][<?php echo $language['language_id']; ?>]" ><?php echo isset($config_menu_item['html_block'][$language['language_id']]) ? $config_menu_item['html_block'][$language['language_id']] : ''; ?></textarea>
													</td>
												</tr>
											</table>	
										</div>
									<?php } ?>
								</div>
									
								<div id="tab-manufacturer-<?php echo $menu_item_row; ?>_options">
									<table class="form">
										<tr class="form-group">
											<td class="col-sm-2 control-label"><?php echo $ns_text_manufacturer; ?></td>
											<td class="col-sm-10">
												<div class="well well-sm">
													<?php foreach($manufacturers_list as $manufacturer){?>
														<div class="checkbox">
															<label>
																<input type="checkbox" name="config_menu_item[<?php echo $menu_item_row; ?>][manufacturers_list][]" <?php if(isset($manufacturers_list_selected[$menu_item_row][$manufacturer['manufacturer_id']])){?> checked="checked" <?php } ?> value="<?php echo $manufacturer['manufacturer_id']?>"/> <?php echo $manufacturer['name']?> 
															</label>
														</div>
													<?php } ?>
												</div>
											</td>
										</tr>
									</table>									
								</div>
								<div id="tab-information-<?php echo $menu_item_row; ?>_options">
									<table class="form">
										<tr class="form-group">
											<td><?php echo $ns_text_information; ?></td>
											<td>
												<div class="well well-sm">
													<?php foreach($informations_list as $information){ ?>
														<div class="checkbox">
															<label>
																<input type="checkbox" name="config_menu_item[<?php echo $menu_item_row; ?>][informations_list][]" <?php if(isset($informations_list_selected[$menu_item_row][$information['information_id']])){?> checked="checked" <?php } ?> value="<?php echo $information['information_id'];?>"/> <?php echo $information['title'];?> 
															</label>
														</div>
													<?php } ?>                
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div id="tab-product-<?php echo $menu_item_row; ?>_options">
									<table class="form">
										<tr>
											<td><?php echo $ns_text_product_width; ?></td>
											<td>
												<input type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][product_width]" value="<?=isset($config_menu_item['product_width'])?$config_menu_item['product_width']:50?>" placeholder="<?php echo $ns_text_product_width; ?>" id="input-product_width" class="form-control" />									 
											</td>
										</tr>		
										<tr>
											<td><?php echo $ns_text_product_height; ?></td>
											<td>
												<input type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][product_height]" value="<?=isset($config_menu_item['product_height'])?$config_menu_item['product_width']:50?>" placeholder="<?php echo $ns_text_product_height; ?>" id="input-product_height" class="form-control" />									 
											</td>
										</tr>		
										<tr>
											<td><?php echo $ns_text_product; ?></td>
											<td>
												<input type="text" name="product" value="" placeholder="<?php echo $ns_text_product; ?>" id="input-product-<?php echo $menu_item_row; ?>" class="form-control" />
												<div id="product-product-<?php echo $menu_item_row; ?>" class="well well-sm">
													<?php if(!empty($config_menu_item['products_list_sel'])) { ?>
														<?php foreach ($config_menu_item['products_list_sel'] as $products_list) { ?>
														<div id="product-item-<?php echo $menu_item_row; ?>-<?php echo $products_list['product_id']; ?>"><i class="btn-danger fa fa-trash-o fa-lg"></i> <?php echo $products_list['name']; ?>
															<input type="hidden" name="config_menu_item[<?php echo $menu_item_row; ?>][products_list][]" value="<?php echo $products_list['product_id']; ?>" />
														</div>
														<?php } ?>
													<?php } ?>
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div id="tab-link-<?php echo $menu_item_row; ?>_options">
									<table class="form">
										<tr>
											<td><?php echo $ns_text_link_options; ?></td>
											<td>
												<select id="input-use_target_blank" name="config_menu_item[<?php echo $menu_item_row; ?>][use_target_blank]" class="form-control">
													<?php if ($config_menu_item['use_target_blank']) { ?>
														<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
														<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $text_enabled; ?></option>
														<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
									</table>
								</div>	
								<div id="tab-add-<?php echo $menu_item_row; ?>-html">
									<table class="form">
										<tr>
											<td><?php echo $ns_text_add_html; ?></td>
											<td>
												<select id="input_use_html_category" name="config_menu_item[<?php echo $menu_item_row; ?>][use_add_html]" class="form-control">
													<?php if ($config_menu_item['use_add_html']) { ?>
													<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>								
											</td>
										</tr>
									</table>
										<div class="tab-add-<?php echo $menu_item_row; ?> htabs" id="language_add_html_<?php echo $menu_item_row; ?>">
											<?php foreach ($languages as $language) { ?>
												<a href="#language_add_html_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
											<?php } ?>
										</div>					
										<?php foreach ($languages as $language) { ?>
											<div id="language_add_html_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>">
												<table class="form">
													<tr>
														<td colspan="2">
															<textarea id="input_add_html_description_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>"  name="config_menu_item[<?php echo $menu_item_row; ?>][add_html][<?php echo $language['language_id']; ?>]" ><?php echo isset($config_menu_item['add_html'][$language['language_id']]) ? $config_menu_item['add_html'][$language['language_id']] : ''; ?></textarea>
														</td>
													</tr>
													</table>
											</div>
										<?php } ?>
								</div>							
							</div>
						</div>

<script>
$(document).ready(function() {	
	sel_type_category('<?php echo $menu_item_row; ?>');
	});
			$('#input-category-<?php echo $menu_item_row; ?>').autocompletemegamenu({
				'source': function(request, response) {
					$.ajax({
						url: 'index.php?route=module/megamenuvh/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
						dataType: 'json',
						success: function(json) {				
							response($.map(json, function(item) {
								return {
									label: item['name'],
									value: item['category_id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					$('#input-category-<?php echo $menu_item_row; ?>').val('');		
					$('#category-item-<?php echo $menu_item_row; ?>-' + item['value']).remove();	
					cathtml1  = '<div class="row-category-menu" id="category-item-<?php echo $menu_item_row; ?>' + item['value'] + '">';
					cathtml1 += '<i class="fa fa-trash-o fa-lg"></i> ';
					cathtml1 += '<input class="form-control input-sort-category" size="3" type="text" name="config_menu_item[<?php echo $menu_item_row; ?>][sort_category][' + item['value'] + ']" value="0"/>';
					cathtml1 += ' <select class="input-sticker-category form-control" name="config_menu_item[<?php echo $menu_item_row; ?>][sticker][' + item['value'] + ']">';
					cathtml1 += '	<option value="0"><?php echo $ns_text_select_sticker;?></option>';					
					cathtml1 += '	<option value="new"><?php echo "NEW";?></option>';
					cathtml1 += '	<option value="top"><?php echo "TOP";?></option>';
					cathtml1 += '	<option value="sale"><?php echo "SALE";?></option>';
					cathtml1 += '</select>';
					cathtml1 += '<span>'+item['label']+'</span>';		
					cathtml1 += '<input type="hidden" name="config_menu_item[<?php echo $menu_item_row; ?>][category_list][]" value="' + item['value'] + '" /></div>';
					
					$('#category-category-<?php echo $menu_item_row; ?>').append(cathtml1);	
				}
			});
			$('#category-category-<?php echo $menu_item_row; ?>').delegate('.fa-trash-o', 'click', function() {
				$(this).parent().remove();
			});

			$('#input-product-<?php echo $menu_item_row; ?>').autocompletemegamenu({
				'source': function(request, response) {
					$.ajax({
						url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
						dataType: 'json',
						success: function(json) {
							response($.map(json, function(item) {
								return {
									label: item['name'],
									value: item['product_id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					$('#input-product-<?php echo $menu_item_row; ?>').val('');		
					$('#product-item-<?php echo $menu_item_row; ?>' + item['value']).remove();		
					$('#product-product-<?php echo $menu_item_row; ?>').append('<div class="proditem" id="product-item-<?php echo $menu_item_row; ?>' + item['value'] + '"><i class="btn-danger fa fa-trash-o fa-lg"></i> ' + item['label'] + '<input type="hidden" name="config_menu_item[<?php echo $menu_item_row; ?>][products_list][]" value="' + item['value'] + '" /></div>');	
				}
			});
			$('#product-product-<?php echo $menu_item_row; ?>').delegate('.fa-trash-o', 'click', function() {
				$(this).parent().remove();
			});
			
			
			
			 

<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('html_description_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
CKEDITOR.replace('input_add_html_description_<?php echo $menu_item_row; ?>_<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
</script>
		<?php $menu_item_row++; ?>
	<?php } ?>
	</div>
</div>
<script type="text/javascript">
	var menu_item_row = <?php echo $menu_item_row; ?>;

	function addItemCategoryBanner() {
				html  = '<div id="tab-menu-item-' + menu_item_row + '">';
				html +='<ul class="general-'+menu_item_row+' htabs list-unstyled">';
				html +='	<li class="active"><a href="#tab-menu-setting-'+ menu_item_row +'" data-toggle="tab"><?php echo $ns_tab_menu_setting; ?></a></li>';
				html +='	<li  class="show_elements_' + menu_item_row +' show_elements_category_'+ menu_item_row +'"><a href="#tab-category-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li> ';
				html +='	<li  class="show_elements_'+ menu_item_row +' show_elements_manufacturer_'+ menu_item_row +'"><a href="#tab-manufacturer-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li>'; 								
				html +='	<li  class="show_elements_'+ menu_item_row +' show_elements_information_'+ menu_item_row +'"><a href="#tab-information-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li>'; 								
				html +='	<li  class="show_elements_'+ menu_item_row +' show_elements_product_'+ menu_item_row +'"><a href="#tab-product-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li>'; 
				html +='	<li  class="show_elements_'+ menu_item_row +' show_elements_link_'+ menu_item_row +'"><a href="#tab-link-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_options; ?></a></li>'; 
				
				html +='	<li  class="show_elements_' + menu_item_row +' show_elements_html_'+ menu_item_row +'"><a href="#tab-html-'+ menu_item_row +'_options" data-toggle="tab"><?php echo $ns_tab_html; ?></a></li> ';
				html +='	<li  class="show_elements_' + menu_item_row +' show_elements_add_html_'+menu_item_row+'"><a href="#tab-add-'+ menu_item_row +'-html" data-toggle="tab"><?php echo $ns_tab_add_html; ?></a></li>';	
											
				
				html +='</ul>';
				
				html +='<div class="tab-content">';
				html +='<div id="tab-menu-setting-'+ menu_item_row +'">';
							
				html +='<table class="form">';
				html +='<tr>';
				html +='	<td><?php echo $ns_text_menu_name;?></td>';
				html +='	<td>';
				html +='		<?php foreach ($languages as $language) { ?>';
				html +='			<div class="input-group pull-left">';
				html +='				<span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>';
				html +='					<input id="namemenu_'+ menu_item_row +'_<?php echo $language['language_id']; ?>" onChange="change_name_menu('+ menu_item_row +','+<?php echo $language['language_id']; ?>+')" class="form-control" type="text" name="config_menu_item['+ menu_item_row +'][namemenu][<?php echo $language['language_id']; ?>]" value="" />';
				html +='			</div>';
				html +='		<?php } ?>';
				html +='	</td>';
				html +='</tr>';
				
				html +='	<tr class="additional_menu">';
				html +='		<td><?php echo $ns_text_additional_menu; ?></td>';
				html +='		<td>';
				html +='			<select name="config_menu_item['+ menu_item_row +'][additional_menu]" id="input-status-additional-menu" class="form-control">';			
				html +='				<option value="additional"><?php echo $text_enabled; ?></option>';
				html +='				<option value="left" selected="selected"><?php echo $text_disabled; ?></option>';
				html +='			</select>';
				html +='		</td>';
				html +='	</tr>';
				
				html +='<tr>';
				html +='	<td><?php echo $ns_text_menu_link;?></td>';
				html +='	<td>';
				html +='		<input class="form-control" type="text" name="config_menu_item['+ menu_item_row +'][link]" value="" />';
				html +='	</td>';
				html +='</tr>';
				
				html +='<tr>';
				html +='	<td><?php echo $ns_text_type;?></td>';
				html +='	<td>';
				html +='		<select onChange="sel_type_category('+menu_item_row+')" name="config_menu_item['+ menu_item_row +'][menu_type]" id="input-menu_type_'+ menu_item_row +'" class="form-control">';
				html +='			<option value="category"><?php echo $ns_text_type_category; ?></option>';
				html +='			<option value="html"><?php echo $ns_text_type_html; ?></option>';
				html +='			<option value="manufacturer"><?php echo $ns_text_type_manufacturer; ?></option>';
				html +='			<option value="information"><?php echo $ns_text_type_information; ?></option>';
				html +='			<option value="product"><?php echo $ns_text_type_product; ?></option>';
				html +='			<option value="link"><?php echo $ns_text_type_link; ?></option>';
				html +='		</select>';
				html +='	</td>';
				html +='</tr>';
				
				html +='	<tr>';
				html +='		<td><?php echo $ns_text_sticker_parent; ?></td>';
				html +='		<td>';
				html +='			<select class="form-control" name="config_menu_item['+ menu_item_row +'][sticker_parent]">';
				html +='				<option value="0"><?php echo $ns_text_select_sticker;?></option>	';				
				html +='				<option value="new"><?php echo "NEW";?></option>';
				html +='				<option value="top"><?php echo "TOP";?></option>';
				html +='				<option value="sale"><?php echo "SALE";?></option>';
				html +='			</select>';
				html +='		</td>';
				html +='	</tr>';
				
				html +='	<tr>';
				html +='		<td><?php echo $ns_text_status; ?></td>';
				html +='		<td>';
				html +='	 	 	<select name="config_menu_item['+ menu_item_row +'][status]" id="input-status" class="form-control">';	
				html +='				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
				html +='				<option value="0"><?php echo $text_disabled; ?></option>';
				html +='			 </select>';
				html +='		</td>';
				html +='	</tr>';
				
				html +='	<tr>';
				html +='		<td><?php echo $ns_text_sort_menu;?></td>';
				html +='		<td>';
				html +='			<input class="form-control" type="text" name="config_menu_item['+ menu_item_row +'][sort_menu]" value="0" />';
				html +='		</td>';
				html +='	</tr>';
				
				html +='	<tr>';
				html +='		<td><?php echo $ns_text_thumb; ?></td>';
				html += '		<td>';
				html += '		<div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb-image' + menu_item_row + '" />';
				html += '		<input type="hidden" name="config_menu_item[' + menu_item_row + '][image]" value="" id="icon-image' + menu_item_row + '" />';
				html += '		<br />';
				html += '		<a onclick="image_upload(\'icon-image' + menu_item_row + '\', \'thumb-image' + menu_item_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb-image' + menu_item_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#icon-image' + menu_item_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
				html +='	</tr>';
				
				html +='</table>';
				html += '</div>';
				
				
				html += '		<div id="tab-category-'+ menu_item_row+'_options">';
				html +='		<table class="form">';
				html += '			<tr>';
				html += '				<td class="col-sm-4 control-label" for="input_variant_category_'+ menu_item_row +'"><?php echo $ns_type_dropdown_list; ?></td>';
				html += '				<td>';
				html += '					<select onChange="sel_type_category('+menu_item_row+')" name="config_menu_item['+ menu_item_row +'][variant_category]" id="input_variant_category_'+ menu_item_row +'" class="form-control">';				
				html += '						<option value="simple"><?php echo $ns_type_dropdown_simple; ?></option>';
				html += '						<option value="full"><?php echo $ns_type_dropdown_full; ?></option>';
				html += '						<option value="full_image"><?php echo $ns_type_dropdown_full_image; ?></option>';
				html += '					</select>';
				html += '				</td>';
				html += '			</tr>';
				
				html += '			<tr>';
				html += '				<td><?php echo $ns_show_sub_categories; ?></td>';
				html += '				<td>';
				html += '					<select id="input-category_show_subcategory" name="config_menu_item['+ menu_item_row +'][show_sub_category]" class="form-control">';						
				html += '						<option value="1"><?php echo $text_enabled; ?></option>';
				html += '						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>';									
				html += '					</select>';	
				html += '				</td>';
				html += '			</tr>';
				html += '			<tr>';
				html += '				<td><?php echo $ns_text_product_width; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="config_menu_item['+ menu_item_row +'][category_img_width]" value="50" placeholder="<?php echo $ns_text_product_width; ?>" class="form-control" />';									 
				html += '				</td>';
				html += '			</tr>';		
				html += '			<tr>';
				html += '				<td><?php echo $ns_text_product_height; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="config_menu_item['+ menu_item_row +'][category_img_height]" value="50" placeholder="<?php echo $ns_text_product_height; ?>" class="form-control" />';									 
				html += '				</td>';
				html += '			</tr>';
				html += '			<tr>';
				html += '				<td><?php echo $ns_text_category_list; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="category" value="" placeholder="<?php echo $ns_text_category; ?>" id="input-category-'+menu_item_row+'" class="form-control" />';
				html += '					<div id="category-category-'+menu_item_row+'" class="well well-sm">';											
				html += '					</div>';
				html += '				</td>';
				html += '			</tr>';
				html +='	</table>';
				html += '	</div>';
				
				
				html += '		<div id="tab-manufacturer-'+menu_item_row+'_options">';
				html +='		<table class="form">';
				html += '			<tr>';
				html += '				<td><?php echo $ns_text_manufacturer; ?></td>';
				html += '				<td>';
				html += '					<div class="well well-sm">';
				html += '						<?php foreach($manufacturers_list as $manufacturer){?>';
				html += '							<div class="checkbox">';
				html += '								<label>';
				html += '									<input type="checkbox" name="config_menu_item['+ menu_item_row +'][manufacturers_list][]"  value="<?php echo $manufacturer['manufacturer_id']?>"/> <?php echo addslashes($manufacturer['name']);?>'; 
				html += '								</label>';
				html += '							</div>';
				html += '						<?php } ?>';
				html += '					</div>';
				html += '				</td>';
				html += '			</tr>';
				html +='		</table>';
				html += '		</div>';
				
				
				
				html += '		<div id="tab-information-'+ menu_item_row +'_options">';
				html +='		<table class="form">';
				html += '			<tr class="form-group">';
				html += '				<td><?php echo $ns_text_information; ?></td>';
				html += '				<td>';
				html += '					<div class="well well-sm">';
				html += '						<?php foreach($informations_list as $information){ ?>';
				html += '							<div class="checkbox">';
				html += '								<label>';
				html += '									<input type="checkbox" name="config_menu_item['+ menu_item_row +'][informations_list][]"  value="<?php echo $information['information_id']?>"/> <?php echo addslashes($information['title']);?>'; 
				html += '								</label>';
				html += '							</div>';
				html += '						<?php } ?>';                
				html += '					</div>';
				html += '				</td>';
				html += '			</tr>';
				html +='		</table>';
				html += '		</div>';
				
				
				html += '		<div id="tab-product-'+ menu_item_row +'_options">';
				html +='		<table class="form">';
				html += '			<tr class="form-group">';
				html += '				<td><?php echo $ns_text_product_width; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="config_menu_item['+ menu_item_row +'][product_width]" value="50" placeholder="<?php echo $ns_text_product_width; ?>" id="input-product_width" class="form-control" />	';								 
				html += '				</td>';
				html += '			</tr>';		
				html += '			<tr class="form-group">';
				html += '				<td><?php echo $ns_text_product_height; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="config_menu_item['+ menu_item_row +'][product_height]" value="50" placeholder="<?php echo $ns_text_product_height; ?>" id="input-product_height" class="form-control" />	';								 
				html += '				</td>';
				html += '			</tr>';		
				html += '			<tr class="form-group">';
				html += '				<td><?php echo $ns_text_product; ?></td>';
				html += '				<td>';
				html += '					<input type="text" name="product" value="" placeholder="<?php echo $ns_text_product; ?>" id="input-product-'+ menu_item_row +'" class="form-control" />';
				html += '						<div id="product-product-'+ menu_item_row +'" class="well well-sm">';																					
				html += '						</div>';
				html += '				</td>';
				html += '			</tr>';
				html +='		</table>';
				html += '		</div>';
				
				
				
				html += '		<div id="tab-link-'+ menu_item_row +'_options">';
				html +='		<table class="form">';
				html += '			<tr>';
				html += '				<td><?php echo $ns_text_link_options; ?></td>';
				html += '				<td>';
				html += '					<select id="input-use_target_blank" name="config_menu_item['+ menu_item_row +'][use_target_blank]" class="form-control">';								
				html += '						<option value="1"><?php echo $text_enabled; ?></option>';
				html += '						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>';
				html += '					</select>';
				html += '				</td>';
				html += '			</tr>';
				html +='		</table>';
				html += '		</div>';
				

				html += '		<div id="tab-html-'+ menu_item_row +'_options">';
				html += '			<div class="tab-html-'+menu_item_row+' htabs" id="language_html_'+ menu_item_row +'">';
				html += '				<?php foreach ($languages as $language) { ?>';
				html += '					<a href="#language_html_'+ menu_item_row +'_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>';
				html += '				<?php } ?>';
				html += '			</div>';				
				html += '			<?php foreach ($languages as $language) { ?>';
				html += '			<div class="tab-pane" id="language_html_'+ menu_item_row +'_<?php echo $language['language_id']; ?>">';
				html +='				<table class="form">';
				html += '					<tr>';
				html += '						<td><?php echo $ns_text_html_description; ?></label>';
				html += '						<td>';
				html += '							<textarea name="config_menu_item['+ menu_item_row +'][html_block][<?php echo $language['language_id']; ?>]" id="html_description_'+ menu_item_row +'_<?php echo $language['language_id']; ?>"></textarea>';
				html += '						</td>';
				html += '					</tr>';
				html +='				</table>';
				html += '			</div>';	
				html += '			<?php } ?>';								
				html += '		</div>';
				
				
				html += '		<div id="tab-add-'+ menu_item_row +'-html">';
				html +='		<table class="form">';
				html += '			<div class="form-group">';
				html += '				<label class="col-sm-4 control-label" for="input_use_html_category"><?php echo $ns_text_add_html; ?></label>';
				html += '				<div class="col-sm-8">';
				html += '					<select id="input_use_html_category" name="config_menu_item['+ menu_item_row +'][use_add_html]" class="form-control">';
				html += '						<option value="1"><?php echo $text_enabled; ?></option>';
				html += '						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>';
				html += '					</select>';
				html += '				</div>';
				html += '			</div>';
				html +='		</table>';
				html += '		<div class="tab-add-'+menu_item_row+' htabs" id="language_add_html_'+ menu_item_row +'">';
				html += '			<?php foreach ($languages as $language) { ?>';
				html += '				<a href="#language_add_html_'+ menu_item_row +'_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>';
				html += '			<?php } ?>';
				html += '		</div>';				
				html += '		<?php foreach ($languages as $language) { ?>';
				html += '			<div class="tab-pane" id="language_add_html_'+ menu_item_row +'_<?php echo $language['language_id']; ?>">';
				html +='				<table class="form">';
				html += '					<tr>';
				html += '						<td>';
				html += '							<textarea name="config_menu_item['+ menu_item_row +'][add_html][<?php echo $language['language_id']; ?>]" id="input_add_html_description_'+ menu_item_row +'_<?php echo $language['language_id']; ?>"></textarea>';
				html += '						</td>';
				html += '					</tr>';
				html +='				</table>';
				html += '			</div>';
				html += '		<?php } ?>';
				html += '		</div>';
				
				html +='</div>';
				html +='</div>';
				$('.megamenu-content').append(html);
				
				$('#addMenuItem').before('<a id="menu-item-' + menu_item_row + '" class="menu-tab-link" href="#tab-menu-item-' + menu_item_row + '" > <?php echo $ns_text_menu_name; ?> <i onclick="$(\'#menu-item-' + menu_item_row + '\').remove(); $(\'#tab-menu-item-' + menu_item_row + '\').remove();  $(\'a[href=#tab-menu-item-0]\').trigger(\'click\'); return false;" class="fa fa-minus-circle remove_menu_item"></i></a>');
				
				$('.vtabs a').tabs();	
				$('.general-'+ menu_item_row +'.htabs a').tabs();
				$('.tab-html-'+ menu_item_row +'.htabs a').tabs(); 
				$('.tab-add-'+ menu_item_row +'.htabs a').tabs(); 
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('html_description_'+ menu_item_row +'_<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
CKEDITOR.replace('input_add_html_description_'+ menu_item_row +'_<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
				$('#menu-item-' + menu_item_row).trigger('click');
				

		var category_row_menu_item = menu_item_row;
		$('#input-category-'+category_row_menu_item).autocompletemegamenu({
				'source': function(request, response) {
					$.ajax({
						url: 'index.php?route=module/megamenuvh/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
						dataType: 'json',
						success: function(json) {				
							response($.map(json, function(item) {
								return {
									label: item['name'],
									value: item['category_id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					$('#input-category-'+category_row_menu_item).val('');		
					$('#category-item-'+ category_row_menu_item +'' + item['value']).remove();
					
					cathtml  = '<div class="row-category-menu" id="category-item-'+ category_row_menu_item +'' + item['value'] + '">';
					cathtml += '<i class="btn btn-danger fa fa-trash-o fa-lg"></i> ';
					cathtml += '<input class="form-control input-sort-category" size="3" type="text" name="config_menu_item['+ category_row_menu_item +'][sort_category][' + item['value'] + ']" value="0"/>';
					cathtml += ' <select class="input-sticker-category form-control" name="config_menu_item['+ category_row_menu_item +'][sticker][' + item['value'] + ']">';
					cathtml += '	<option value="0"><?php echo $ns_text_select_sticker;?></option>';					
					cathtml += '	<option value="new"><?php echo "NEW";?></option>';
					cathtml += '	<option value="top"><?php echo "TOP";?></option>';
					cathtml += '	<option value="sale"><?php echo "SALE";?></option>';
					cathtml += '</select>';
					cathtml += '<span>'+item['label']+'</span>';
					cathtml += '<input type="hidden" name="config_menu_item['+ category_row_menu_item +'][category_list][]" value="' + item['value'] + '" /></div>';
					$('#category-category-'+ category_row_menu_item+'').append(cathtml);		
				}
		});	
		$('#category-category-'+ category_row_menu_item).delegate('.fa-trash-o', 'click', function() {
			$(this).parent().remove();
		});	
				
				
				/*PRODUCT AUTOCOMPLETE*/
		var product_row_menu_item = menu_item_row;
		$('#input-product-'+ product_row_menu_item).autocompletemegamenu({
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['product_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('#input-product-'+ product_row_menu_item).val('');	
				$('#product-item-'+ product_row_menu_item +'' + item['value']).remove();		
				$('#product-product-'+ product_row_menu_item+'').append('<div class="proditem" id="product-item-'+ product_row_menu_item +'' + item['value'] + '"><i class="btn-danger fa fa-trash-o fa-lg"></i> ' + item['label'] + '<input type="hidden" name="config_menu_item['+ product_row_menu_item +'][products_list][]" value="' + item['value'] + '" /></div>');	
			}
		});
		$('#product-product-'+ product_row_menu_item).delegate('.fa-trash-o', 'click', function() {
			$(this).parent().remove();
		});
							
				
		$(".show_elements_"+menu_item_row).hide();
		sel_menu_type=$("#input-menu_type_"+ menu_item_row +" :selected").val();
		sel_category_type=$("#input_variant_category_"+ menu_item_row +" :selected").val();
		$(".show_elements_"+sel_menu_type+'_'+menu_item_row).show();
		if((sel_menu_type=="category" && (sel_category_type=="simple" || sel_category_type=="0")) || sel_menu_type=="0" || sel_menu_type=="auth" || sel_menu_type=="link" || sel_menu_type=="html")
		  {
			$(".show_elements_add_html_"+menu_item_row).hide();
		  }
		else
		  {
			$(".show_elements_add_html_"+menu_item_row).show();
		  }
				  
				  
		var config_main_menu_selection = $('#input-menu-selection option:selected').val();	
		change_menu_theme(config_main_menu_selection);
	
			
	menu_item_row++;
	}

	function change_name_menu(menu_item_row, lang_id){
		namemenu=$('#namemenu_'+ menu_item_row+'_'+lang_id+'').val();
		$('#menu-item-' + menu_item_row).html(namemenu +' <i onclick="$(\'#menu-item-' + menu_item_row + '\').remove(); $(\'#tab-menu-item-' + menu_item_row + '\').remove(); return false;" class="fa fa-minus-circle remove_menu_item"></i></a></li>');
	}
			
	function sel_type_category(menu_item_row){
		$(".show_elements_"+menu_item_row).hide();	
		sel_menu_type=$("#input-menu_type_"+ menu_item_row +" :selected").val();		
		sel_category_type=$("#input_variant_category_"+ menu_item_row +" :selected").val();
		$(".show_elements_"+sel_menu_type+'_'+menu_item_row).show();
		if((sel_menu_type=="category" && (sel_category_type=="simple" || sel_category_type=="0")) || sel_menu_type=="0" || sel_menu_type=="auth" || sel_menu_type=="link" || sel_menu_type=="html")
		  {
			$(".show_elements_add_html_"+menu_item_row).hide();
		  }
		else
		  {
			$(".show_elements_add_html_"+menu_item_row).show();
		  }	
	}
			</script>			
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('.vtabs a').tabs();
//--></script> 

<script type="text/javascript"><!--
<?php $menu_item_rowtabs = 0; ?>
<?php foreach ($config_menu_items as $config_menu_item) { ?>						
	$('.general-<?php echo $menu_item_rowtabs; ?>.htabs a').tabs();
	$('.tab-html-<?php echo $menu_item_rowtabs; ?>.htabs a').tabs(); 
	$('.tab-add-<?php echo $menu_item_rowtabs; ?>.htabs a').tabs(); 
	<?php $menu_item_rowtabs++; ?>
<?php } ?>
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<?php echo $footer; ?>