<?php echo $header; ?>
<div id="content">
   <div class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
             <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
          <?php } ?>
   </div>
   <?php if ($error_warning) { ?>
          <div class="warning"><?php echo $error_warning; ?></div>
       <?php } ?>
   <div class="box">
      <div class="heading">
         <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
         <div class="buttons"><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_nazad; ?></a></div>
      </div>
      <div class="content">
    <?php echo $text_todo; ?>
      </div>
      2014 (c) Izyum Web System www.izyum.name 
   </div>
</div>
<?php echo $footer; ?>