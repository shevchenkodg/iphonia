<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="warning deactivation" style="display:none"></div>
  <div class="success" style="display:none"></div>
<div class="box">
    <div class="heading">
     <h1> <?php echo $heading_title_activation; ?></h1>
    </div>
    <div class="content">
<div style="text-align:center;">

			<div class="block-form">
				<form id="myForm" method="post">
				<div style="text-align:center;">
				<div class="title-activation"><?php echo $add_activation_key;?></div>
				<input id="license_key" name="license_key" class="input form-control-fastorder" type="text"/><br>
				<button id="submit1" class="btn-add" name="submitButton"><?php echo $activated_btn;?></button></div>
				</form>
			</div>
<script type="text/javascript">
$('#submit1').bind('click',function() {
function test() {	
var license_key = $('#license_key').val();
	$.post('index.php?route=module/megamenuvh/activation&token=<?php echo $token; ?>', 'license_key=' + license_key);
	}
});
</script>

			<div class="block-form">
				<form id="deactivation" method="post">
				<div style="text-align:center;">
				<div class="title-activation"><?php echo $enter_deactivation_key;?></div>
				<input id="license_key_deactivation" name="license_key_deactivation" class="input form-control-fastorder" type="text"/><br>
				<a class="btn-add" id="submitChangeStatus"><?php echo $btn_deactivation;?></a>
				</form>
			</div>
			</div>
			<script type="text/javascript">
			$('#submitChangeStatus').bind('click',function() {
				var license_key_deactivation = $('#license_key_deactivation').val();
				var success = 'false';
					$.ajax({
						type:'get',
						dataType:'json',
						data:'license_key_deactivation=' + license_key_deactivation,
						url:'index.php?route=module/megamenuvh/deactivation&token=<?php echo $token; ?>',			
						success: function(json){
						console.log(json);
							if (json['error']) {
							$('.alert-success').hide('slow');
								$('.deactivation').html(json['error']);
								$('.deactivation').fadeIn('slow');
							}
							if (json['success']) {
							$('.deactivation').hide('slow');
							$('.alert.alert-success').html(json['success']);
							$('.alert.alert-success').fadeIn('slow');
							}
							
							
						}
					});				
			
			});
			</script>
</div>
<style>
.block-form{
	width:300px;
	display:inline-block;
	margin:0px 100px;
}
.btn-add {
	background: #11A2E0 none repeat scroll 0 0;
	border-color: #11A2E0 #11A2E0 #0383BA;
	border-image: none;
	border-radius: 4px;
	border-style: solid;
	border-width: 1px 1px 2px;
	color: #fff;
	padding: 7px 14px;
	text-decoration: none;
	font-size:14px;
	display:inline-block;
}
.btn-add:hover, .btn-add:focus {
	background: #0383BA none repeat scroll 0 0;
	border-color: #0383BA;
}	
input.form-control-fastorder {
	  background-color: #fff;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 3px;
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	  color: #555;
	  display: inline-block;
	  font-size: 12px;
	  height: 25px;
	  line-height: 1.42857;
	  padding: 3px 7px !important;
	  transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
	  width: 80%;
	  margin: 4px 0px;
}
.title-activation {
	font-size:14px;
	color:#000;
}
#myForm label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	color: red;
    font-style: italic;
}
#myForm input.error {
    border: 1px dotted red;
}
#deactivation label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
	color: red;
    font-style: italic;
}
#deactivation input.error {
    border: 1px dotted red;
}
</style>       
	</div>
</div>
</div>
<?php echo $footer; ?>
