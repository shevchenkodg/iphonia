<?php echo $header;?>
	<div id="content">
		<div class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } ?>
		</div>
		<div class="content">
			<form action="<?php $action; ?>" method="post" enctype="multipart/form-data" id="form_license">
				<table class="form">
					<tr>
						<td><?php echo $text_license; ?></td>
						<td><textarea type="text" name="license" placeholder="<?php echo $entry_license; ?>"></textarea></td>
					</tr>
					<tr>
						<td><input type="submit" class="button" value="<?php echo $button_submit; ?>"></td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>

<?php echo $footer; ?>
