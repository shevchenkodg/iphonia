<?php echo $header; ?>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
	  
	  <a href="<?php echo $color_kits; ?>" class="button"><?php echo $button_edit_kits; ?></a>
	  <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
	  <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
	  
	  </div>
    </div>
    <div class="content" style="overflow: inherit;">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	
	<h2><?php echo $text_settings ?></h2>
	<p>
		<label><?php echo $text_title ?></label>
		  <?php foreach ($languages as $language) { ?><br />
		  <input type="text" name="color_kit[title][<?php echo $language['language_id']; ?>]" value="<?php echo isset($title[$language['language_id']]) ? $title[$language['language_id']] : ''; ?>" />
		  <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
		  <?php } ?>
	</p>
	<p>
	<label><?php echo $text_status_visibility ?></label> <input name="color_kit[name]" value="<?php echo $name ?>" style="width: 35px" />
	</p>
	<p>
	<label><?php echo $text_limit_colors ?></label> <input name="color_kit[visible]" value="<?php echo $visible ?>" style="width: 35px" />
	</p>
	<p>
	<label><?php echo $text_limit_colors_popup ?></label> <input name="color_kit[limit]" value="<?php echo $limit ?>" style="width: 35px"/>
	</p>
	<p>
	<label><?php echo $text_size ?></label> <input name="color_kit[width]" value="<?php echo $width ?>" style="width: 35px"/><?php echo $text_width ?>
	<input name="color_kit[height]" value="<?php echo $height ?>" style="width: 35px"/><?php echo $text_height ?>
	</p>
	<p>
	<label><?php echo $text_ico_size ?></label> <input name="color_kit[ico_width]" value="<?php echo $ico_width ?>" style="width: 35px"/><?php echo $text_width ?>
	<input name="color_kit[ico_height]" value="<?php echo $ico_height ?>" style="width: 35px"/><?php echo $text_height ?>
	</p>
	  <p>
		  <label><?php echo $text_enable_popup ?></label> <input name="color_kit[enable_popup]" value="<?php if(isset($enable_popup)){ echo $enable_popup; } else { echo '1';} ?>" style="width: 35px" />
	  </p>
	<h2><?php echo $text_color_templates ?></h2>
	   <table id="option-value" class="list">
          <thead>
            <tr>
              <td class="left"><span class="required">*</span> <?php echo $text_entry_name; ?></td>
              <td class="left"><?php echo $text_entry_color; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>	
		  
		  <?php $option_value_row = 0; ?>
		  <?php if(isset($color_options)) { ?>
			<?php foreach ($color_options as $option_value) { ?>
			<tbody id="option-value-row<?php echo $option_value_row; ?>">
            <tr>
              <td class="left"><input type="hidden" name="option_value[<?php echo $option_value_row; ?>][option_id]" value="<?php echo $option_value['option_id']; ?>" />
			     <?php foreach ($languages as $language) { ?>
                <input type="text" name="option_value[<?php echo $option_value_row; ?>][r_opt_description][<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_value['r_opt_description'][$language['language_id']]) ? $option_value['r_opt_description'][$language['language_id']]['name'] : ''; ?>" />
                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php if (isset($error_option_value[$option_value_row][$language['language_id']])) { ?>
                <span class="error"><?php echo $error_option_value[$option_value_row][$language['language_id']]; ?></span>
                <?php } ?>
                <?php } ?>
				
			  </td>
			  
			<td class="left"><input class="input-colors" type="text" name="option_value[<?php echo $option_value_row; ?>][color]" value="<?php echo $option_value['color']; ?>" /></td>
			<td class="right"><input type="text" name="option_value[<?php echo $option_value_row; ?>][sort]" value="<?php echo $option_value['sort']; ?>" size="1" /></td>	
			<td class="left"><a onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>			
			</tr>  
			 </tbody> 
			 <?php $option_value_row++; ?>
			  <?php } ?>
			<tfoot>
            <tr>
              <td colspan="3"></td>
              <td class="left"><a onclick="addOption();" class="button"><?php echo $button_add; ?></a></td>
            </tr>
          </tfoot>
			
		  <?php } ?>
		</table>  
		  
		
	  </form>
	</div>
  </div>
	
</div>
<script type="text/javascript"><!--
var option_value_row = <?php echo $option_value_row; ?>;

function addOption() {
	html  = '<tbody id="option-value-row' + option_value_row + '">';
	html += '  <tr>';	
    html += '    <td class="left"><input type="hidden" name="option_value[' + option_value_row + '][option_id]" value="" />';
	<?php foreach ($languages as $language) { ?>
	html += '    <input type="text" name="option_value[' + option_value_row + '][r_opt_description][<?php echo $language['language_id']; ?>][name]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '</td>';
	html += '<td class="left"><input type="text" class="input-colors" name="option_value[' + option_value_row + '][color]" value="" /></td>';
	html += '<td class="right"><input type="text" name="option_value[' + option_value_row + '][sort]" value="" size="1" /></td>';
	html += '<td class="left"><a onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '</tr>';	
    html += '</tbody>';
	
	$('#option-value tfoot').before(html);
	addColor();
	option_value_row++;
}
function addColor() {
	$('.input-colors').each( function() {
		$(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    defaultValue: $(this).attr('data-defaultValue') || '',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: $(this).attr('data-letterCase') || 'lowercase',
                    opacity: $(this).attr('data-opacity'),
                    position: $(this).attr('data-position') || 'top left',
                    change: function(hex, opacity) {
                        var log;
                        try {
                            log = hex ? hex : 'transparent';
                            if( opacity ) log += ', ' + opacity;

                        } catch(e) {}
                    },
			theme: 'default'
		});
	});
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready( function() {
addColor();
});
//--></script>
<?php echo $footer; ?>