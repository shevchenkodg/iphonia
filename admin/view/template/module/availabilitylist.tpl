<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<?php echo $breadcrumb['separator']; ?>
				<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<div class="box">	
		<div class="heading">
			<h1><img src="view/image/product.png" alt="" /> <?php echo $text_list; ?></h1>
			<div class="buttons">
				<a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
			</div>
		</div>
		
		<div class="content">
			<div id="tabs" class="htabs">
				<a href="#tab-active"><?php echo $tab_active; ?></a>
				<a href="#tab-closed"><?php echo $tab_closed; ?></a>
			</div>
			<form action="<?php echo $action;?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-active">
					<div class="box">
                        <div class="heading">
                            <div class="buttons clearfix">                                            
                                <a class="button notify"><?php echo $button_send; ?></a>
								<a class="button update"><?php echo $reload; ?></a>
							</div>
						</div>
					</div>
					<table class="form">
						<tr style="width:100%">
							<td style="width:5%"><?php echo $id; ?></td>
							<td style="width:12%"><?php echo $text_time; ?></td>
							<td style="width:15%"><?php echo $text_product; ?></td>
							<td style="width:10%"><?php echo $text_price; ?></td>
							<td style="width:15%"><?php echo $text_mail; ?></td>
							<td style="width:12%"><?php echo $text_name; ?></td>
							<td style="width:12%"><?php echo $text_comment; ?></td>
							<td style="width:7%"><?php echo $text_statuse; ?></td>
							<td style="width:5%"><?php echo $text_quantity; ?></td>
						</tr>
						
						<?php if(isset($availabilities)) {
							foreach ($availabilities as $availability) { ?>
							<tr>
								<td><?php echo $availability['id']; ?></td>
								<td><?php echo $availability['time']; ?></td>
								<td><?php echo $availability['product']; ?>
									<?php 
										foreach($availability['options'] as $index => $value ) {
											echo "<p>" . $value['option_type'] . " - " . $value['option_name'] . " - " . $value['option_quantity'] . "</p>";
											
										}
									?>
								</td>
								<td><?php echo $availability['price']; ?></td>
								<td><?php echo $availability['email']; ?></td>
								<td><?php echo $availability['name']; ?></td>
								<td><?php echo $availability['comment']; ?></td>
								<td>
									<select id="<?php echo $availability['id']; ?>" class="mail_status">
										<?php foreach($statuses as $key => $status) { ?>
											<?php $selected=($key==$availability['status'])?'selected':''; ?>
											<option value='<?php echo $key; ?>' data-id="<?php echo $availability['id']; ?>" data-product-id="<?php echo $availability['product_id']; ?>"<?php echo $selected; ?> ><?php echo $status; ?> </option>
										<?php } ?>
									</select>
								</td>
								<td><?php echo $availability['quantity']; ?></td>
							</tr>
						<?php }
						} ?>
					</table>
				</div>
				<div id="tab-closed">
					<div class="box">
                        <div class="heading">
                            <div class="buttons clearfix">
								<a class="button update"><?php echo $reload; ?></a>
								<a id="delete" class="button"><?php echo $button_delete; ?></a>
							</div>
						</div>
					</div>
					<table class="form">
						<tr style="width:100%">
							<td style="width:5% !important"><input type="checkbox" id="checkall"></td>
							<td style="width:5%"><?php echo $id; ?></td>
							<td style="width:12%"><?php echo $text_time; ?></td>
							<td style="width:12%"><?php echo $text_product; ?></td>
							<td style="width:10%"><?php echo $text_price; ?></td>
							<td style="width:12%"><?php echo $text_mail; ?></td>
							<td style="width:10%"><?php echo $text_name; ?></td>
							<td style="width:10%"><?php echo $text_comment; ?></td>
							<td style="width:7%"><?php echo $text_statuse; ?></td>
							<td style="width:5%"><?php echo $text_quantity; ?></td>
						</tr>
						<?php if (isset($processed)) {
							foreach ($processed as $mail) { ?>
							<tr style="width:100%">
								<td style="width:5%"><input type="checkbox" name="selected[]" data-id="<?php echo $mail['id'];?>"></td>
								<td style="width:5%"><?php echo $mail['id']; ?></td>
								<td style="width:12%"><?php echo $mail['time']; ?></td>
								<td style="width:12%"><?php echo $mail['product']; ?>
									<?php 
										foreach($mail['options'] as $index => $value ) {
											echo "<p>" . $value['option_type'] . " - " . $value['option_name'] . " - " . $value['option_quantity'] . "</p>";										
										}
									?>
								</td>
								<td style="width:10%"><?php echo $mail['price']; ?></td>
								<td style="width:12%"><?php echo $mail['email']; ?></td>
								<td style="width:10%"><?php echo $mail['name']; ?></td>
								<td style="width:10%"><?php echo $mail['comment']; ?></td>
								<td style="width:7%">
									<select id="<?php echo $mail['id']; ?>" class="mail_status">
										<?php foreach($statuses as $key => $status) { ?>
											<?php $selected=($key==$mail['status'])?'selected':''; ?>
											<option value='<?php echo $key; ?>' data-id="<?php echo $mail['id']; ?>" data-product-id="<?php echo $mail['product_id']; ?>"<?php echo $selected; ?> ><?php echo $status; ?> </option>
										<?php } ?>
									</select>
								</td>
								<td style="width:5%"><?php echo $mail['quantity']; ?></td>
							</tr>
					<?php }
						} ?>
					</table>
				</div>
			</form>
		</div>
		
	</div>
</div>

<?php echo $footer; ?>	
<script type="text/javascript">	
	$('#tabs a').tabs(); 
	$('#languages a').tabs(); 
	$('#vtab-option a').tabs();	
</script>
<script type="text/javascript">	
	$(document).ready(function() {
    $('.notify').on('click', function() {
		$.ajax({
			url:'index.php?route=module/avail/notify&token=<?php echo $token; ?>',
			type:'post',
			dataType:'json'			
		})
			.success (function(message) {
				if (message['success']) {					
					$(".alert").remove();
					$(".breadcrumb").after("<div class='success'>" + message['success'] + "</div>");
					$('html, body').animate({ scrollTop: 0 }, 'slow');
					location.reload();
				} else {					
					$(".warning").remove();
					$(".breadcrumb").after("<div class='warning'>" + message['error'] + "</div>");
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}				
			})
			.error (function(message) {				
				$(".warning").remove();
				$(".breadcrumb").after("<div class='warning'>" + message + "</div>");
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			});
    });
	
	$('.mail_status').change(function() {
            $this = $(this);			
            $.ajax({
                url: "index.php?route=module/avail/changeMailStatus&token=<?php echo $token;?>",
                type: 'post',
                data: 'id=' + $(this).attr('id') + '&status=' + $(this).children('option:selected').val(),				
                success: function(r) {					
                    if (r == 'ok') {
                        $this.css('background-color', 'rgb(167, 245, 147)');
                    } else {
                        $this.css('background-color', 'rgb(245, 147, 147)');
                    }

                }
            });
        });
	$(".update").click(function(){		
        location.reload();
    });
	$("#checkall").click(function(){
		$("input[name*=selected]").prop("checked", this.checked);
	});
	$("#delete").click(function(e){
		e.preventDefault();
		var idArray = [];
		$("input[name*=selected]:checked").each(function(index, element){
			idArray.push($(element).data('id'));
		});			
		$.ajax({
			url: "index.php?route=module/avail/deleteNotifications&token=<?php echo $token;?>",
			type: 'post',
			data: {idArray:idArray}		
		})
		.success(function(message){			
			location.reload();
		})
		.error(function(message){
			$(".alert").remove();
			$(".breadcrumb").after("<div class='alert alert-danger'>" + message + "</div>");
			$('html, body').animate({ scrollTop: 0 }, 'slow');
		});
	});
});
</script>