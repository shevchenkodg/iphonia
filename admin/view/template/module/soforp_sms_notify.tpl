<?php echo $header; ?>

<?php
require_once(DIR_SYSTEM . '/engine/soforp_view.php' );
$widgets = new SoforpWidgets('soforp_sms_notify_',$params);
$widgets->text_select_all = $text_select_all;
$widgets->text_unselect_all = $text_unselect_all;

function button($action,$name) { ?>
<tr><td colspan="2">
        <a class="button" href="<?php echo $action; ?>"><span><?php echo $name; ?></span></a>
    </td>
</tr>
<?php } ?>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <style>
        textarea {
            border: 1px solid #DDDDDD;
        }

        .messages textarea {
            width:98%;
            margin: 8px 0 7px 4px;
        }
    </style>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if (isset($success) && $success) { ?>
    <div class="success" style="display: none;"><?php echo $success; ?></div>
    <script>jQuery(".success").slideDown().delay(1000).slideUp();</script>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>

            <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>

            <div class="buttons">
                <?php if( !isset($license_error) ) { ?>
                <a onclick="$('#form').attr('action', '<?php echo $save; ?>');
                        $('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
                <a onclick="$('#form').attr('action', '<?php echo $save_and_close; ?>');
                        $('#form').submit();" class="button"><span><?php echo $button_save_and_close; ?></span></a>
                <?php } else { ?>
                <a onclick="location = '<?php echo $recheck; ?>';" class="button"><?php echo $button_recheck; ?></a>
                <?php } ?>
                <a onclick="location = '<?php echo $close; ?>';" class="button"><?php echo $button_close; ?></a>
            </div>
        </div>
        <div class="content">
            <form action="<?php echo $close; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="tabs" class="htabs">
                    <a href="#tab-general"><?php echo $tab_general; ?></a>
                    <?php if( !isset($license_error) ) { ?>
                    <a href="#tab-templates"><?php echo $tab_templates; ?></a>
                    <a href="#tab-fields"><?php echo $tab_fields; ?></a>
                    <a href="#tab-logs"><?php echo $tab_logs; ?></a>
                    <?php } ?>
                    <a href="#tab-support"><?php echo $tab_support; ?></a>
                    <a href="#tab-license"><?php echo $tab_license; ?></a>
                </div>

                <div id="tab-general">
                    <?php if( !isset($license_error) ) { ?>
                    <table class="form">
                        <?php $widgets->dropdown('status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                        <?php $widgets->dropdown('force',array( 0 => $text_disabled, 1 => $text_enabled)); ?>

                        <?php $widgets->input('recipients'); ?>
                        <?php $widgets->input('align'); ?>
                        <?php $widgets->dropdown('gate', $gates); ?>
                        <?php $widgets->input('gate_login'); ?>
                        <?php $widgets->input('gate_password'); ?>
                        <?php $widgets->input('gate_sender'); ?>
                        <?php $widgets->textarea('gate_additional'); ?>

                        <tr>
                            <td><?php echo $entry_gate_check; ?></td>
                            <td>
                                <?php echo $entry_gate_check_phone; ?> <input id="phone" value=""/>
                                <?php echo $entry_gate_check_message; ?> <input id="message" value=""/>
                                <button id="gate_test_button" class="btn" onclick="checkGate(); return false;"><?php echo $entry_gate_check; ?></button>
                            </td>
                        </tr>
                    </table>

                    <?php } else { ?>
                    <div><?php echo $license_error; ?></div>
                    <?php } ?>
                </div>

                <?php if( !isset($license_error) ) { ?>

                <div id="tab-templates">
                    <h3><?php echo $text_admin_templates; ?></h3>
                    <p><?php echo $tab_templates_desc ?></p>
                    <table class="list display messages" id="items-table">
                        <thead>
                            <tr>
                                <td style="width:30%" class="left"><?php echo $column_status_name; ?></td>
                                <td><?php echo $column_template_subject; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($soforp_sms_notify_templates as $id => $template) {
                            if( $id > 0 )
                            continue;
                            ?>
                            <tr>
                                <td class="left"><?php echo $template["name"] ?></td>
                                <td><textarea name="soforp_sms_notify_templates[<?php echo $id; ?>][subject]"><?php echo $template['subject']; ?></textarea></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                    <h3><?php echo $text_customer_templates; ?></h3>
                    <p><?php echo $tab_templates_desc ?></p>
                    <table class="list display messages" id="items-table">
                        <thead>
                            <tr>
                                <td style="width:30%" class="left"><?php echo $column_status_name; ?></td>
                                <td><?php echo $column_template_subject; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($soforp_sms_notify_templates as $id => $template) {
                            if( $id < 0 )
                            continue;
                            ?>
                            <tr>
                                <td class="left"><?php echo $template["name"] ?></td>
                                <td><textarea style="width:98%" name="soforp_sms_notify_templates[<?php echo $id; ?>][subject]"><?php echo $template['subject']; ?></textarea></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>

                <?php if( !isset($license_error) ) { ?>
                <div id="tab-fields">
                    <table class="list display" id="items-table" width="30%">
                        <thead>
                            <tr>
                                <td width="200px" class="left"><?php echo $entry_field_list_name; ?></td>
                                <td><?php echo $entry_field_list_desc; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach( $fields as $field_name => $field_desc ) { ?>
                            <tr>
                                <td class="left">{<?php echo $field_name ?>}</td>
                                <td><?php echo $field_desc ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php }?>


                <?php if( !isset($license_error) ) { ?>
                <div id="tab-logs">
                    <?php $widgets->debug_and_logs('debug',array( 0 => $text_disabled, 1 => $text_enabled), $clear,
                    $button_clear_log ); ?>
                    <textarea style="width: 98%; height: 300px; padding: 5px; border: 1px solid #CCCCCC; background: #FFFFFF; overflow: scroll;"><?php echo $logs; ?></textarea>
                </div>
                <?php } ?>

                <div id="tab-support">
                    <?php echo $mail_support; ?>
                </div>

                <div id="tab-license">
                    <?php echo $module_licence; ?>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $('#tabs a').tabs();
    window.token = '<?php echo $token; ?>';
    function checkGate() {
        var data = {
            gate: $("#soforp_sms_notify_gate").val(),
            login: $("#soforp_sms_notify_gate_login").val(),
            password: $("#soforp_sms_notify_gate_password").val(),
            sender: $("#soforp_sms_notify_gate_sender").val(),
            additional: $("#soforp_sms_notify_gate_additional").val(),
            phone: $("#phone").val(),
            message: $("#message").val()
        };
        $.ajax({
            url: 'index.php?route=module/soforp_sms_notify/check&token=' + window.token,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (json) {
                $('.success, .warning, .attention, .information').remove();

                if (json['status'])
                    alert('Сообщение отправлено успешно');
                else
                    alert('При отправке сообщения произошла ошибка');
            }
        });
    }
    //--></script>
<?php echo $footer; ?>