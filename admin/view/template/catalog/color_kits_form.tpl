<?php echo $header; ?>
<style>
.table>tbody>tr>td{vertical-align: middle !important;}
h1{font-size:21px !important;margin: 0px !important;}
#menu > ul li ul {overflow: inherit !important;}
#menu > ul li li a {width: 157px;font-size: 13px;}
</style>
<div id="content" class="container-fluid">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="panel panel-default">
   <div class="panel-heading">
		<div class="row">
			<div class="col-md-8">
				<h1><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <?php echo $heading_title; ?></h1>
			</div> 
			<div class="col-md-4">
				<div class="pull-right">
					<a onclick="$('#form').submit();" class="btn btn-info btn-sm"><?php echo $button_save; ?></a> &nbsp <a href="<?php echo $cancel; ?>" class="btn btn-info btn-sm"><?php echo $button_cancel; ?></a>
				</div> 
			</div> 
		</div> 

    </div>
    <div class="panel-body">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form"  class="form-inline">
		<div class="row">
            <div class="form-group col-xs-12">
				<label><span class="required">*</span> <?php echo $entry_group; ?></label>
					<input type="text" name="name" class="form-control" value="<?php echo $color_kit['name'] ?>">			
			</div>
		</div><p></p><div class="row">
            <div class="form-group col-xs-12">
				<label><?php echo $column_status; ?></label>
				<select  name="status" class="form-control">
				<?php if($color_kit['status'] == 1){ ?>
					<option value="1" selected><?php echo $status_on ?></option>
					<option value="0"><?php echo $status_off ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $status_on ?></option>
					<option value="0" selected><?php echo $status_off ?></option>
				<?php }?>  
				</select>

			</div>
    </div><p></p><div class="row">
            <div class="form-group col-xs-12">
				<label><?php echo $template ?></label>
				<select  name="tpl" class="form-control">
				<?php $tpl = (isset($color_kit['tpl'])) ? $color_kit['tpl'] : '';?>
					<option value="color" <?php if($tpl == 'color'){ ?>selected<?php } ?>><?php echo $template_colors ?></option>
					<option value="photos" <?php if($tpl == 'photos'){ ?>selected<?php } ?>>Фото товаров</option>
					<option value="img" <?php if($tpl == 'img'){ ?>selected<?php } ?>><?php echo $template_photos ?></option>
				</select>

			</div>
    </div>
	<p></p>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title">
				<?php echo $entry_name_product ?>
					<input type="hidden" name="product_id" value="" />
					<div class="form-group">
						<input type="text" name="product" class="form-control">
					</div>
					<div class="form-group">
				 Model
					<input type="text" name="model" class="form-control">	
					</div>
					<div class="form-group">
				 Sku
					<input type="text" name="sku" class="form-control">
					</div>
				</h3>
			  </div>
			  <div class="panel-body">
				
				<table id="product-list" class="table table-bordered table-responsive">
					<thead>
						<tr>
							<th><?php echo $name_product ?></th>
							<th><?php echo $name_color ?></th>
							<th><?php echo $column_photo ?></th>
							<th></th>
						</tr>
					</thead>
					<?php $product_row = 0; ?>
					<?php foreach ($color_kits as $color_kit) { ?>
					<tbody id="product-row<?php echo $product_row; ?>">					
						<tr>
							<td><strong><input type="hidden" name="color_kit[<?php echo $product_row; ?>][product_id]" value="<?php echo $color_kit['product_id'] ?>" /><?php echo $color_kit['product_name'] ?></strong></td>
							<td>
							<select name="color_kit[<?php echo $product_row; ?>][option_id]" class="form-control">
							<?php foreach ($get_colors as $color) { ?>
								<?php if($color['option_id'] == $color_kit['option_id']) { ?>
									<option value="<?php echo $color['option_id'] ?>" selected><?php echo $color['name'] ?></option>
								<?php } else { ?>
									<option value="<?php echo $color['option_id'] ?>"><?php echo $color['name'] ?></option>
								<?php } ?>
							<?php } ?>	
							</select>
							</td>
							<td>
								<div class="image">
								<img src="/image/<?php echo $color_kit['image']; ?>" width="33px" height="33px" alt="" id="thumb<?php echo $product_row; ?>" />
								<input type="hidden" name="color_kit[<?php echo $product_row; ?>][image]" value="<?php echo $color_kit['image']; ?>" id="image<?php echo $product_row; ?>"  />
								</div><br /> 
								<a onclick="image_upload('image<?php echo $product_row; ?>', 'thumb<?php echo $product_row; ?>');"><?php echo $choose ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $product_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $product_row; ?>').attr('value', '');"><?php echo $clear ?></a>
							</td>
							<td><a onclick="$('#product-row<?php echo $product_row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>
						</tr>
					</tbody>
					<?php $product_row++; ?>
					<?php } ?>
					
					<tfoot>
					</tfoot>
				</table>	
				
			  </div> 
			</div>
		</div>
	</div>
        
      </form>
    </div>
  </div>
</div>


<script type="text/javascript"><!--
var product_row = <?php echo $product_row; ?>;
$('input[name=\'product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/color_kits/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id,
						model: item.model,
						sku: item.sku
					
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		
			html  = '<tbody id="product-row' + product_row + '">';
			html += '<tr>';	
			html += '<td><input type="hidden" name="color_kit[' + product_row + '][product_id]" value="' + ui.item['value'] + '" /><strong>' + ui.item['label'] + '</strong></td>';
			html += '<td><select name="color_kit[' + product_row + '][option_id]" class="form-control">';
				<?php foreach ($get_colors as $color) { ?>
					html += '<option value="<?php echo $color['option_id'] ?>"><?php echo $color['name'] ?></option>';
				<?php } ?>	
			html +='</select></td>';	
			html += '<td><div class="image"><img src="<?php echo $no_image; ?>" width="33px" height="33px" alt="" id="thumb' + product_row + '" /><input type="hidden" name="color_kit[' + product_row + '][image]" value="" id="image' + product_row + '" /></div></br><a onclick="image_upload(\'image' + product_row + '\', \'thumb' + product_row + '\');"><?php echo $choose ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + product_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + product_row + '\').attr(\'value\', \'\');"><?php echo $clear ?></a></td>';
			html += '<td><a onclick="$(\'#product-row' + product_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';		
			html += '</tr>';	
			html += '</tbody>';	
			$('#product-list tfoot').before(html);
			product_row++;
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});	
$('input[name=\'sku\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/color_kits/autocomplete&token=<?php echo $token; ?>&filter_sku=' + encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id,
						model: item.model,
						sku: item.sku
					
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
	
			html  = '<tbody id="product-row' + product_row + '">';
			html += '<tr>';	
			html += '<td><input type="hidden" name="color_kit[' + product_row + '][product_id]" value="' + ui.item['value'] + '" /><strong>' + ui.item['label'] + '</strong></td>';
			html += '<td><select name="color_kit[' + product_row + '][option_id]" class="form-control">';
				<?php foreach ($get_colors as $color) { ?>
					html += '<option value="<?php echo $color['option_id'] ?>"><?php echo $color['name'] ?></option>';
				<?php } ?>	
			html +='</select></td>';
			
			html += '<td><div class="image"><img src="<?php echo $no_image; ?>" width="33px" height="33px" alt="" id="thumb' + product_row + '" /><input type="hidden" name="color_kit[' + product_row + '][image]" value="" id="image' + product_row + '" /></div></br><a onclick="image_upload(\'image' + product_row + '\', \'thumb' + product_row + '\');"><?php echo $choose ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + product_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + product_row + '\').attr(\'value\', \'\');"><?php echo $clear ?></a></td>';
			
			html += '<td><a onclick="$(\'#product-row' + product_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';		
			html += '</tr>';	
			html += '</tbody>';	
			$('#product-list tfoot').before(html);
			product_row++;
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});	
$('input[name=\'model\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/color_kits/autocomplete&token=<?php echo $token; ?>&filter_model=' + encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id,
						model: item.model,
						sku: item.sku				
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
	
			html  = '<tbody id="product-row' + product_row + '">';
			html += '<tr>';	
			html += '<td><input type="hidden" name="color_kit[' + product_row + '][product_id]" value="' + ui.item['value'] + '" /><strong>' + ui.item['label'] + '</strong></td>';
			html += '<td><select name="color_kit[' + product_row + '][option_id]" class="form-control">';
				<?php foreach ($get_colors as $color) { ?>
					html += '<option value="<?php echo $color['option_id'] ?>"><?php echo $color['name'] ?></option>';
				<?php } ?>	
			html +='</select></td>';	
			html += '<td><div class="image"><img src="<?php echo $no_image; ?>" width="33px" height="33px" alt="" id="thumb' + product_row + '" /><input type="hidden" name="color_kit[' + product_row + '][image]" value="" id="image' + product_row + '" /></div></br><a onclick="image_upload(\'image' + product_row + '\', \'thumb' + product_row + '\');"><?php echo $choose ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + product_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + product_row + '\').attr(\'value\', \'\');"><?php echo $clear ?></a></td>';
			html += '<td><a onclick="$(\'#product-row' + product_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';		
			html += '</tr>';	
			html += '</tbody>';	
			$('#product-list tfoot').before(html);
			product_row++;
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});	
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + thumb).replaceWith('<img src="' + data + '" alt="" width="24px" height="24px" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<?php echo $footer; ?>