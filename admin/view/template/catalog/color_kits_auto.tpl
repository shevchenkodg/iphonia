<?php echo $header; ?>
<style>
.table>tbody>tr>td{vertical-align: middle !important;}
h1{font-size:21px !important;margin: 0px !important;}
#menu > ul li ul {overflow: inherit !important;}
#menu > ul li li a {width: 157px;font-size: 13px;}
</style> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if(isset($success)) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  
   <div class="panel panel-default">
     <div class="panel-heading">
		<div class="row">
			<div class="col-md-8">
				<h1><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <?php echo $heading_title; ?></h1>
			</div> 
			<div class="col-md-4">
				<div class="pull-right">
					<a href="<?php echo $kits_auto; ?>" class="btn btn-default btn-sm" role="button"><?php echo $button_auto; ?></a> &nbsp
					<a href="<?php echo $color_list; ?>" class="btn btn-default btn-sm" role="button"><?php echo $button_color_list; ?></a> &nbsp
					<a href="<?php echo $insert; ?>" class="btn btn-default btn-sm" role="button"><?php echo $button_sets ?></a>
				</div> 
			</div> 
		</div> 

    </div>
    <div class="panel-body">
		<form action="<?php echo $action ?>" method="POST" class="form-inline">

		<?php echo $text_found ?><strong><?php echo count($products); ?></strong>
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<ul class="list-group" style="overflow-y: scroll;height: 100%;max-height:800px;">
				<?php foreach($products as $key => $product){ ?>
				  <li class="list-group-item">
					  <input name="colorkit[<?=$key?>][name_group]" value="<?=$key?>" type="hidden">

					<span class="badge"><?php echo count($product) ?></span>
					  <span class="glyphicon glyphicon-remove del_group" style="color: #F00" aria-hidden="true"></span>
					  <strong><?php echo $key ?></strong>

					<div>
						<?php foreach($product as $item){ ?>
							<input name="colorkit[<?=$key?>][rows][<?=$item['product_id']?>][product_id]" value="<?=$item['product_id']?>" type="hidden">
							<input name="colorkit[<?=$key?>][rows][<?=$item['product_id']?>][option_id]" value="<?=$item['option_id']?>" type="hidden">
							--- <?php echo $item['product_name'] ?> [<strong><?php echo $item['color'] ?></strong>]<br />
						<?php } ?>	
					 </div>
				  </li>
				 <?php } ?> 
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">

				<div class="form-group">
					<label><?php echo $column_status ?>: </label>
					<input name="status" class="form-control" style="width: 50px;" value="1">
				</div>
				<div class="form-group">
					<label>Применить шаблон: </label>
					<select name="template" class="form-control">
						<option value="color">Цвета</option>
						<option value="photos">Фото товаров</option>
					</select>
				</div>
					<button class="btn btn-success" type="submit"><?php echo $button_add_auto ?></button>

			</div>
		</div>
	</form>
</div>
</div></div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<?php echo $footer; ?>
<script>
	$(".del_group").on("click",function(){
		$(this).parent().remove();
	})
</script>