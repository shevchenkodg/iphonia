<?php

$soforp_extension_shutdown_redirect = "";
function soforp_extension_shutdown()
{
	global $soforp_extension_shutdown_redirect;
	if ($soforp_extension_shutdown_redirect)
		header("location: " . $soforp_extension_shutdown_redirect);
}


class SoforpController extends Controller
{
	protected $_moduleSysName = "soforp_";
	protected $_logFile = "error.txt";
	protected $debug = false;

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_logFile = $this->config->get("config_error_filename");
		if (isset($this->request->get["route"])) {
			$route = explode('/', $this->request->get["route"]);
			if( $route[0] == 'extension' ) {
				$this->_route = $route[1];
			} else {
				$this->_route = $route[0];
			}
		} else {
			$this->_route = 'module';
		}
	}

	public function checkLicense()
	{
		global $soforp_extension_shutdown_redirect;

		if (!function_exists('ioncube_file_info')) {
			return $this->ioncube();
		}

		$soforp_extension_shutdown_redirect = str_replace("&amp;", "&", $this->url->link($this->_route . '/' . $this->_moduleSysName . '/license', 'token=' . $this->session->data['token'], 'SSL'));
		register_shutdown_function('soforp_extension_shutdown');
		require_once(DIR_APPLICATION . "controller/tool/" . $this->_moduleSysName . ".php");
		$soforp_extension_shutdown_redirect = "";
	}

	public function initButtons($data){

		$data['save'] = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');
		$data['save_and_close'] = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'] . "&close=1", 'SSL');
		$data['recheck'] = $this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'], 'SSL');
		$data['close'] = $this->url->link('extension/' . $this->_route, 'token=' . $this->session->data['token'], 'SSL');
		$data['clear'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/clear', 'token=' . $this->session->data['token'], 'SSL');
		$data['download'] = $this->url->link($this->_route . '/' . $this->_moduleSysName . '/downloadLogFile', 'token=' . $this->session->data['token'], 'SSL');

		return $data;
	}

	public function install()
	{
		$this->load->model($this->_route . "/" . $this->_moduleSysName);
		$this->{"model_" . $this->_route . "_" . $this->_moduleSysName}->install();
	}

	public function uninstall()
	{
		$this->load->model($this->_route . "/" . $this->_moduleSysName);
		$this->{"model_" . $this->_route . "_" . $this->_moduleSysName}->uninstall();
	}

	protected function debug($message)
	{
		$this->log($message);
	}

	protected function log($message)
	{
		if (!$this->debug)
			return;
		file_put_contents(DIR_LOGS . $this->_logFile, date("Y-m-d H:i:s - ") . $message . "\r\n", FILE_APPEND);
	}


	protected function initLanguage($module)
	{
		$this->data = array_merge($this->data, $this->language->load($module));
	}


	protected function getLogs($limit = 10000)
	{
		$result = "";
		if (is_file(DIR_LOGS . $this->_logFile)) {
			$file = fopen(DIR_LOGS . $this->_logFile, "r");
			fseek($file, -$limit, SEEK_END);
			$result = fread($file, $limit);
			fclose($file);
		}
		return $result;
	}

	public function clearLogs()
	{
		if (is_file(DIR_LOGS . $this->_logFile)) {
			$f = fopen(DIR_LOGS . $this->_logFile, "w");
			fclose($f);
		}
		$this->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName, 'token=' . $this->session->data['token'].'#tab-logs', 'SSL'));
	}

	protected function initBreadcrumbs($items)
	{
		$newItems = array_merge(array(array("common/home", "text_home")), $items);

		$this->data['breadcrumbs'] = array();

		foreach($newItems as $item) {
			if (isset($this->session->data['token'])) {
				$this->data['breadcrumbs'][] = array('href' => $this->url->link($item[0], 'token=' . $this->session->data['token'], 'SSL'), 'text' => $this->language->get($item[1]), 'separator' => (count($this->data['breadcrumbs']) == 0 ? FALSE : ' :: '));
			} else {
				$this->data['breadcrumbs'][] = array('href' => $this->url->link($item[0], 'SSL'), 'text' => $this->language->get($item[1]), 'separator' => (count($this->data['breadcrumbs']) == 0 ? FALSE : ' :: '));
			}
		}
	}

	protected function initParams($items)
	{

		foreach($items as $item) {
			if (isset($this->request->post[$item[0]])) {
				$this->data[$item[0]] = $this->request->post[$item[0]];
			} else if ($this->config->has($item[0])) {
				$this->data[$item[0]] = $this->config->get($item[0]);
			} else if (isset($item[1])) {
				$this->data[$item[0]] = $item[1]; // default value
			}
		}
	}

	protected function initParamsList($items)
	{

		foreach($items as $item) {
			if (isset($this->request->post[$this->_moduleSysName . "_" . $item])) {
				$this->data[$this->_moduleSysName . "_" . $item] = $this->request->post[$this->_moduleSysName . "_" . $item];
			} else if ($this->config->has($this->_moduleSysName . "_" . $item)) {
				$this->data[$this->_moduleSysName . "_" . $item] = $this->config->get($this->_moduleSysName . "_" . $item);
			} else {
				$this->data[$this->_moduleSysName . "_" . $item] = '';
			}
		}
	}

	protected function initConfigParams($items)
	{
		foreach($items as $item) {
			if (!is_array($item))
				$item = array($item);
			$param_name = $item[0];
			$config_name = isset($item[1]) ? $item[1] : $item[0];

			$this->data[$param_name] = $this->config->get($config_name);
		}
	}

	protected function initSessionParams($items)
	{
		foreach($items as $item) {
			if (!is_array($item))
				$item = array($item);
			$param_name = $item[0];
			$session_name = isset($item[1]) ? $item[1] : $item[0];
			$default_value = isset($item[2]) ? $item[2] : '';

			if (isset($this->session->data[$session_name])) {
				$this->data[$param_name] = $this->session->data[$session_name];
			} else {
				$this->data[$param_name] = $default_value;
			}
		}
	}

	protected function addThemeStyle($file)
	{
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/' . $file)) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/' . $file);
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/' . $file);
		}
	}


	protected function renderTemplateOnly($file)
	{
		if (defined('HTTP_CATALOG')) {
			$this->template = $file;
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $file)) {
				$this->template = $this->config->get('config_template') . '/template/' . $file;
			} else {
				$this->template = 'default/template/' . $file;
			}
		}

		$children = $this->children;
		$this->children = array();

		$result = $this->render();

		$this->children = $children;

		return $result;
	}

	protected function renderTemplate($file, $children = array())
	{
		if (defined('HTTP_CATALOG')) {
			$this->template = $file;
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $file)) {
				$this->template = $this->config->get('config_template') . '/template/' . $file;
			} else {
				$this->template = 'default/template/' . $file;
			}
		}

		$this->children = $children;

		return $this->render();
	}

	protected function outputTemplate($file, $children = array())
	{
		if (defined('HTTP_CATALOG')) {
			$this->template = $file;
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $file)) {
				$this->template = $this->config->get('config_template') . '/template/' . $file;
			} else {
				$this->template = 'default/template/' . $file;
			}
		}

		$this->children = $children;

		$this->response->setOutput($this->render());
	}

	protected function outputJson($data)
	{
		$this->response->setOutput(json_encode($data));
	}

}