<?php

class SoforpWidgets
{

	public $nameSpace = ""; // i.e. 'soforp_backup_'
	public $params = ""; // i.e. array('soforp_backup_status' => 1 );
	public $text_select_all = "";
	public $text_unselect_all = "";

	public function __construct($nameSpace, $params)
	{
		$this->nameSpace = $nameSpace;
		$this->params = $params;
	}

	public function dropdown($property, $options)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<select name="<?php echo $this->nameSpace . $property; ?>"
					id="<?php echo $this->nameSpace . $property; ?>">
					<?php foreach($options as $value => $name) { ?>
						<?php if ($this->params[$this->nameSpace . $property] == $value) { ?>
							<option value="<?php echo $value; ?>" selected="selected"><?php echo $name; ?></option>
						<?php } else { ?>
							<option value="<?php echo $value; ?>"><?php echo $name; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</td>
		</tr>
		<?php
	}

	public function dropdownA($property, $array, $index, $options)
	{
		$field_id = $array . '-' . $property . '-' . $index;
		?>
		<div class="form-group" id="field_<?php echo $field_id; ?>" style="display: inline-block; width: 100%;">
			<div class="col-sm-5">
				<label class="control-label"
					for="<?php echo $this->nameSpace . $field_id; ?>"><?php echo $this->params['entry_' . $property]; ?></label>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</div>
			<div class="col-sm-7">
				<select name="<?php echo $array . '[' . $index . '][' . $property . ']'; ?>"
					id="<?php echo $this->nameSpace . $field_id; ?>"
					class="form-control">
					<?php foreach($options as $value => $name) { ?>
						<?php if ($this->params[$array][$index][$property] == $value) { ?>
							<option value="<?php echo $value; ?>" selected="selected"><?php echo $name; ?></option>
						<?php } else { ?>
							<option value="<?php echo $value; ?>"><?php echo $name; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
		<?php
	}

	public function debug_and_logs($property, $options, $clear, $button_clear_log)
	{
		?>
		<table class="form">
			<tr>
				<td><?php echo $this->params['entry_' . $property]; ?></td>
				<td>
					<select name="<?php echo $this->nameSpace . $property; ?>"
						id="<?php echo $this->nameSpace . $property; ?>"
						class="form-control">
						<?php foreach($options as $value => $name) { ?>
							<?php if ($this->params[$this->nameSpace . $property] == $value) { ?>
								<option value="<?php echo $value; ?>" selected="selected"><?php echo $name; ?></option>
							<?php } else { ?>
								<option value="<?php echo $value; ?>"><?php echo $name; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</td>
				<td>
					<div class="buttons" style="float: right"><a
							onclick="$('#form').attr('action', '<?php echo $clear; ?>');
								$('#form').attr('target', '_self');
								$('#form').submit();"
							class="button delete"><span><?php echo $button_clear_log; ?></span></a></div>
				</td>
			</tr>
		</table>
		<?php
	}

	public function input($property)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<input name="<?php echo $this->nameSpace . $property; ?>"
					id="<?php echo $this->nameSpace . $property; ?>" class="form-control"
					value="<?php echo $this->params[$this->nameSpace . $property]; ?>"/>
			</td>
		</tr>
		<?php
	}

	public function inputImage($property, $img, $text_browse, $text_clear, $no_image)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<div class="image"><img src="<?php echo $img; ?>" alt="" id="thumb-logo-<?php echo $property; ?>"/>
					<input type="hidden" name="<?php echo $this->nameSpace . $property; ?>"
						value="<?php echo $this->params[$this->nameSpace . $property]; ?>"
						id="logo-<?php echo $property; ?>"/>
					<br/>
					<a onclick="image_upload('logo-<?php echo $property; ?>', 'thumb-logo-<?php echo $property; ?>');"><?php echo $text_browse ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a onclick="$('#thumb-logo-<?php echo $property; ?>').attr('src', '<?php echo $no_image; ?>');
						$('#logo-<?php echo $property; ?>').attr('value', '');"><?php echo $text_clear ?></a></div>
			</td>
		</tr>

		<?php
	}

	public function inputA($property, $array, $index)
	{ // Arrays
		$field_id = $array . '-' . $property . '-' . $index;
		?>
		<div class="form-group" id="field_<?php echo $field_id; ?>" style="display: inline-block; width: 100%;">
			<div class="col-sm-5">
				<label class="control-label"
					for="<?php echo $this->nameSpace . $field_id; ?>"><?php echo $this->params['entry_' . $property]; ?></label>
				<br>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo $this->params['entry_' . $property . '_desc']; ?>
			</div>
			<div class="col-sm-7">
				<input name="<?php echo $array . '[' . $index . '][' . $property . ']'; ?>"
					id="<?php echo $field_id; ?>" class="form-control"
					value="<?php echo $this->params[$array][$index][$property]; ?>"/>
			</div>
		</div>
		<?php
	}

	public function localeInput($property, $languages)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<?php foreach($languages as $language) { ?>
					<input name="<?php echo $this->nameSpace . $property; ?>[<?php echo $language['language_id']; ?>]"
						id="<?php echo $this->nameSpace . $property . $language['language_id']; ?>" size="50"
						value="<?php echo $this->params[$this->nameSpace . $property][$language['language_id']]; ?>"/>
					<img src="view/image/flags/<?php echo $language['image']; ?>"
						title="<?php echo $language['name']; ?>">
					<br>
				<?php } ?>
			</td>
		</tr>
		<?php
	}

	function password($property)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<input type="password" name="<?php echo $this->nameSpace . $property; ?>"
					id="<?php echo $this->nameSpace . $property; ?>"
					value="<?php echo $this->params[$this->nameSpace . $property]; ?>"/>
			</td>
		</tr>
		<?php
	}

	public function textarea($property, $rows = 6)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
                <textarea rows="<?php echo $rows; ?>" style="width:98%"
					name="<?php echo $this->nameSpace . $property; ?>"
					id="<?php echo $this->nameSpace . $property; ?>"><?php echo $this->params[$this->nameSpace . $property]; ?></textarea>
			</td>
		</tr>
		<?php
	}

	public function textareaA($property, $array, $index, $rows = 6)
	{
		$field_id = $array . '-' . $property . '-' . $index;
		?>
		<div class="form-group <?php echo $property; ?>" id="field_<?php echo $field_id; ?>"
			style="display: inline-block; width: 100%;">
			<div class="col-sm-5">
				<label class="control-label"
					for="<?php echo $field_id; ?>"><?php echo $this->params['entry_' . $property]; ?></label>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</div>
			<div class="col-sm-7">
                <textarea name="<?php echo $array . '[' . $index . '][' . $property . ']'; ?>"
					id="<?php echo $field_id; ?>"
					rows="<?php echo $rows; ?>"
					class="form-control"><?php echo $this->params[$array][$index][$property]; ?></textarea>
			</div>
		</div>
		<?php
	}

	public function text($property)
	{
		?>
		<tr>
			<td>
				<?php echo $this->params['entry_' . $property]; ?>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			</td>
			<td>
				<?php echo $this->params[$this->nameSpace . $property]; ?>
			</td>
		</tr>
		<?php
	}

	public function textA($property, $array, $index)
	{
		$field_id = $array . '-' . $property . '-' . $index;
		?>
		<div class="form-group <?php echo $property; ?>" id="field_<?php echo $field_id; ?>"
			style="display: inline-block; width: 100%;">
			<div class="col-sm-5">
				<label class="control-label"
					for="<?php echo $this->nameSpace . $field_id; ?>"><?php echo $this->params['entry_' . $property]; ?></label>
				<br>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo $this->params['entry_' . $property . '_desc']; ?>
			</div>
			<div class="col-sm-7">
				<?php echo $this->params[$array][$index][$property]; ?>
			</div>
		</div>
		<?php
	}

	public function checklist($property, $options)
	{
		?>
		<tr>
			<td><label><?php echo $this->params['entry_' . $property]; ?></label>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			<td>
				<div class="scrollbox">
					<?php $class = 'odd'; ?>
					<?php foreach($options as $value => $name) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div class="<?php echo $class; ?>">
							<input type="checkbox" id="<?php echo $this->nameSpace . $property . $value; ?>"
								name="<?php echo $this->nameSpace . $property; ?>[]"
								value="<?php echo $value; ?>"<?php if (in_array($value, $this->params[$this->nameSpace . $property]))
								echo ' checked="checked"'; ?> />
							<label
								for="<?php echo $this->nameSpace . $property . $value; ?>"><?php echo $name; ?></label>
						</div>
					<?php } ?>
				</div>
				<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $this->text_select_all; ?></a>
				/
				<a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $this->text_unselect_all; ?></a>
			</td>
		</tr>
		<?php
	}

	public function checklistA($property, $array, $index, $options)
	{
		$field_id = $array . '-' . $property . '-' . $index;
		?>
		<div class="form-group store" id="field_<?php echo $field_id; ?>" style="display: inline-block; width:100%">
			<div class="col-sm-5">
				<label class="control-label"
					for="<?php echo $this->nameSpace . $field_id; ?>"><?php echo $this->params['entry_' . $property]; ?></label>
				<br>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo $this->params['entry_' . $property . '_desc']; ?>
			</div>
			<div class="col-sm-7">
				<div class="well well-sm" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach($options as $value => $name) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
						<div class="<?php echo $class; ?>">
							<input type="checkbox"
								name="<?php echo $array . '[' . $index . '][' . $property . '][]'; ?>"
								value="<?php echo $value; ?>"
								<?php if (in_array($value, $this->params[$array][$index][$property]))
									echo ' checked="checked"'; ?> />
							<?php echo $name; ?>
						</div>
					<?php } ?>
				</div>
				<button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', true);"
					class="btn btn-primary"><i class="fa fa-pencil"></i> <?php echo $this->text_select_all; ?></button>
				<button type="button" onclick="$(this).parent().find(':checkbox').prop('checked', false);"
					class="btn btn-danger"><i class="fa fa-trash-o"></i> <?php echo $this->text_unselect_all; ?>
				</button>
			</div>
		</div>
		<?php
	}

	public function checkbox($property)
	{
		?>
		<tr>
			<td><label><?php echo $this->params['entry_' . $property]; ?></label>
				<?php if (isset($this->params['entry_' . $property . '_desc']))
					echo "<br><br><small><i>" . $this->params['entry_' . $property . '_desc'] . "</i></small>"; ?>
			<td>
				<input type="checkbox" name="<?php echo $this->nameSpace . $property; ?>"
					value="<?php echo $this->params[$this->nameSpace . $property]; ?>"<?php if ($this->params[$this->nameSpace . $property])
					echo ' checked="checked"'; ?> />
			</td>
		</tr>
		<?php
	}

}
