<?php

/**
 * Класс для работы с сервисом Sms.ru
 */
class SmsRu
{
	const REQUEST_SUCCESS = 'success';
	const REQUEST_ERROR = 'error';

	public $login = "";
	public $password = "";
	public $sender = false;
	public $message = "";
	public $phone = "";
	public $debug = false;
	public $_logFile = "soforp_sms_notify.log";

	protected function log( $message ){
		if(!$this->debug)
			return;
		file_put_contents(DIR_LOGS . $this->_logFile, date("Y-m-d H:i:s - ") . "sms.ru: " . $message . "\r\n", FILE_APPEND );
	}

	public function send() {

		$this->log($this->sender . ": " . $this->phone . " => " . $this->message );

		$data = array(
			'api_id'	=> $this->login,
			'from'	=> $this->sender,
			'to'	=> $this->phone,
			'text'	=> $this->message
		);

		$p=base64_decode('cGFydG5lcj1vc3N0b3Jlc21zYnl0ZWhhbmQm');
		$url = "http://sms.ru/sms/send?". http_build_query($data);

		$result = @file_get_contents($url);

		$this->log("ответ сервера: " . print_r($result, true));

		return $result;
	}


}