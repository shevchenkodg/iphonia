<?php
// Heading 
$_['heading_title']         = 'История заявок на уведомление';

// Text
$_['text_account']          = 'Личный Кабинет';
$_['text_order']            = 'заявка';
$_['text_order_id']         = '№ Заказа:';
$_['text_comment']          = 'Комментарий к заказу';
$_['text_empty']            = 'Вы еще не оставляли заявок на уведомлении о поступлении товара!';
$_['text_emptycl']            = 'У вас нету закрытых заявок!';

$_['tab_active']            = 'Активные заявки!';
$_['tab_closed']            = 'Закрытые заявки!';

// Column
$_['column_avail_id']       = '№ Заявки';
$_['column_product']        = 'Название';
$_['column_name']           = 'Наименование товара';
$_['column_price']          = 'Цена';
$_['column_date_added']     = 'Добавлено';
$_['column_status']         = 'Статус заказа';
$_['column_comment']        = 'Комментарий';

$_['button_view']        = 'Страница товара';

?>