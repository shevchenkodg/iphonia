<?php
// Заголовок
$_['heading_title'] 		= 'Поиск';
$_['heading_blog'] 			= 'Блог';


$_['text_search']   		= 'Продукты, соответствующие критериям поиска';
$_['text_keyword'] 			= 'Ключевые слова';
$_['text_category'] 		= 'Все категории';
$_['text_sub_category'] 	= 'Искать в подкатегориях';
$_['text_critea'] 			= 'Критерии поиска';
$_['text_empty'] 			= 'Нет статей, соответствующих критериям поиска.';
$_['text_quantity'] 		= 'Кол-во:';
$_['text_manufacturer'] 	= 'Бренд:';
$_['text_model'] 			= 'Код продукта:';
$_['text_points'] 			= 'Бонусные очки:';
$_['text_price'] 			= 'Цена:';
$_['text_tax'] 				= 'Ex Tax:';
$_['text_reviews'] 			= 'На основе отзывов от% s.';
$_['text_compare'] 			= 'Сравнение продуктов (% s)';
$_['text_display'] 			= 'Показать:';
$_['text_list'] 			= 'Список';
$_['text_grid'] 			= 'Сетка';
$_['text_sort'] 			= 'Сортировать по:';
$_['text_default'] 			= 'По умолчанию';
$_['text_name_asc'] 		= 'Имя (A - Z)';
$_['text_name_desc'] 		= 'Имя (Z - A)';
$_['text_price_asc'] 		= 'Цена (низкая / высокая)';
$_['text_price_desc']		= 'Цена (Высокий / Низкий)';
$_['text_rating_asc'] 		= 'Рейтинг (самый низкий)';
$_['text_rating_desc'] 		= 'Рейтинг (максимум)';
$_['text_model_asc'] 		= 'Модель (A - Z)';
$_['text_model_desc'] 		= 'Модель (Z - A)';
$_['text_limit'] 			= 'Показать:';

// Запись
$_['entry_search'] = 'Поиск:';
$_['entry_description'] = 'Искать в содержимом статьи';
?>