<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="content">
			<div id="tabs" class="htabs">
				<a href="#tab-active"><?php echo $tab_active; ?></a>
				<a href="#tab-closed"><?php echo $tab_closed; ?></a>
			</div>
			<div id="tab-active">
		    <?php  if (!empty($orders)) { ?>			
				<?php foreach ($orders as $order) { ?>					
					 <div class="order-list">
						<div class="order-id"><b><?php echo $column_avail_id;; ?></b> #<?php echo $order['avail_id']; ?></div>
						<div class="order-content">
						  <div><b><?php echo $column_date_added; ?></b> <?php echo $order['date_added']; ?><br />
							<b><?php echo $column_name; ?></b> <?php echo $order['product_name']; ?><br />
							<b><?php echo $column_price; ?></b> <?php echo $order['price']; ?></div>
						  <div><b><?php echo $column_comment; ?></b> <?php echo $order['comment']; ?></div>
						  <div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;</div>
						</div>
					  </div>
					  <?php } ?>
					  <div class="pagination"><?php echo $pagination; ?></div>
					  <?php } else { ?>
					  <div class="content"><?php echo $text_empty; ?></div>
					  <?php } ?>					
			</div>
			<div id="tab-closed">           
				<?php  if (!empty($orderscl)) { ?>				
					<?php foreach ($orderscl as $order) { ?>					
					    <div class="order-list">
							<div class="order-id"><b><?php echo $column_avail_id;; ?></b> #<?php echo $order['avail_id']; ?></div>
							<div class="order-content">
							  <div><b><?php echo $column_date_added; ?></b> <?php echo $order['date_added']; ?><br />
								<b><?php echo $column_name; ?></b> <?php echo $order['product_name']; ?><br />
								<b><?php echo $column_price; ?></b> <?php echo $order['price']; ?></div>
							  <div><b><?php echo $column_comment; ?></b> <?php echo $order['comment']; ?></div>
							  <div class="order-info"><a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;</div>
							</div>
					    </div>
					  <?php } ?>
					  <div class="pagination"><?php echo $paginationcl; ?></div>
				<?php } else { ?>
					  <div class="content"><?php echo $text_empty; ?></div>
				<?php } ?>
					
			</div>
	</div>	
	<div class="buttons">
		<div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
	</div>
</div>		

      <?php echo $content_bottom; ?></div>

<script type="text/javascript">	
$.fn.tabs = function() {
	var selector = this;
	
	this.each(function() {
		var obj = $(this); 
		
		$(obj.attr('href')).hide();
		
		$(obj).click(function() {
			$(selector).removeClass('selected');
			
			$(selector).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).show();
			
			return false;
		});
	});

	$(this).show();
	
	$(this).first().click();
};

	$('#tabs a').tabs(); 
	$('#languages a').tabs(); 
	$('#vtab-option a').tabs();	
</script>
<?php echo $footer; ?>