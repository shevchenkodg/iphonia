<?php $count_for_col = $colors_cfg['limit']; 
$count_colors = count($colors);
for ($i = 0; $i <= $count_colors;) {
$results = array_slice($colors,$i,$count_for_col); ?> 

<div class="box-product" id="popupColorKits">
	<?php foreach ($results as $color) { ?>
      <div class="<?php echo $color['quantity']?>">
        <?php if ($color['thumb']) { ?>
        <div class="image"><a href="<?php echo $color['href']; ?>"><img src="<?php echo $color['thumb']; ?>" alt="<?php echo $color['name_color']; ?>" /></a></div>
        <?php } ?>
        <div class="name">
          <?php if(isset($color['quantity'])){ ?>
          <div class="hideQuantity">Нет в наличии</div>
          <?php } ?>
          <a href="<?php echo $color['href']; ?>"><?php echo $color['name_color']; ?></a></div>
        <?php if ($color['price']) { ?>
        <div class="price">
          <?php if (!$color['special']) { ?>
          <?php echo $color['price']; ?> 
          <?php } else { ?>
          <span class="price-old"><?php echo $color['price']; ?></span> <span class="price-new"><?php echo $color['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
	<?php } ?>
</div>

<?php $i += $count_for_col; } ?>