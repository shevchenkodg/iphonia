<?php
class ControllerAccountAvail extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/avail');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/order', $url, 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['heading_title'] = $this->language->get('heading_title');


		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_emptycl'] = $this->language->get('text_emptycl');

		$this->data['column_avail_id'] = $this->language->get('column_avail_id');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_comment'] = $this->language->get('column_comment');
		$this->data['column_name'] = $this->language->get('column_product');
		$this->data['column_price'] = $this->language->get('column_price');
		
		$this->data['tab_active'] = $this->language->get('tab_active');
		$this->data['tab_closed'] = $this->language->get('tab_closed');

		$this->data['button_view'] = $this->language->get('button_view');
		$this->data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['orders'] = array();

		$this->load->model('account/avail');
		
        $logged_id = $this->customer->getId()?$this->customer->getId():'';
		
		$order_total = $this->model_account_avail->getTotalAvailOp($logged_id);
		$order_totalcl = $this->model_account_avail->getTotalAvailCl($logged_id);
		$results = $this->model_account_avail->getAvailList(($page - 1) * 10, 10);
        
		if($results) {
			foreach ($results as $result) {	

				$this->data['orders'][] = array(
					'avail_id'   => $result['id'],
					'product_name'   => $result['product_name'],
					'status'     => $result['status'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['time'])),
					'price'      => $result['price'],
					'href'       => $result['link_page'],
					'comment'       => $result['comment'],

				);
			}
		}
		$close = $this->model_account_avail->getAvailListClose(($page - 1) * 10, 10);

		if($close){
			foreach ($close as $result) {	

				$this->data['orderscl'][] = array(
					'avail_id'   => $result['id'],
					'product_name'   => $result['product_name'],
					'status'     => $result['status'],
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['time'])),
					'price'      => $result['price'],
					'href'       => $result['link_page'],
					'comment'       => $result['comment'],

				);
			}
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;		
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/avail', 'page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$pagination = new Pagination();
		$pagination->total = $order_totalcl;		
		$pagination->page = $page;
		$pagination->limit = 10;
		
		$pagination->url = $this->url->link('account/avail', 'page={page}', 'SSL');

		$this->data['paginationcl'] = $pagination->render();
		
		$this->data['resultscl'] = sprintf($this->language->get('text_pagination'), ($order_totalcl) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_totalcl - 10)) ? $order_totalcl : ((($page - 1) * 10) + 10), $order_totalcl, ceil($order_totalcl / 10));

		
		
		
		
		$this->data['continue'] = $this->url->link('account/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/avail.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/avail.tpl';
		} else {
			$this->template = 'default/template/account/avail.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());
	}
}